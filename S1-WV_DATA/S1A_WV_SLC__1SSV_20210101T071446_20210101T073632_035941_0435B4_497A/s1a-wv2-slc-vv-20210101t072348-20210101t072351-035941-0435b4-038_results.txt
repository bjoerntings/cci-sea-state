corner points
-27.58966255    -17.65628242
-27.63284492    -17.83050728
-27.77658653    -17.61292839
-27.81996536    -17.7870903
========== RESULTS WAVE VERSION 16.10.2018 ====================
IMAGE SIZE  X: 4466     Y:4782    col:row     POLARIZATION:VV
COVERAGE    X: 20.34243457km       Y: 19.85999712km 
RESOLUTION dX: 4.55495624m       dY:4.153073426m  
resolution of original product 4.55495624m x 4.153073426m;   raster step x:5998.877368m  
===============================================================
FLAGS: H=HIGH PROBABILITY SEA STATE, I=LAND(ISLAND), O=OUTLIER NO SEA STATE, N=NO WIND AVAILABLE, SEA STATE NOT VISIBLE, M=MEAN PROBABILITY SEA STATE, L= LOW PROBABILITY SEA STATE
Filtering wave length
 integration  Lmin=1m
 integration  Lmax=2200m
  peak        Lmin=100m
               FFT=1024pixels, 4664.27519m 
   mean-Hs (SWH_c)=0.502272644m
         mean-wind=11.96069908m/s
--------------0--------------1---------2---------3---------4---------5---------6---------7---------8---------9--------10--------11--------12--------13--------14--------15--------16--------17--------18--------19------------------20--------21--------22--------23--------24--------25--------26--------27--------28--------29--------30--------31--------32--------33--------34--------35--------36--------37--------38--------39--------40--------41--------42--------43--------44--------45--------46--------47--------48--------49--------50--------51--------52--------53--------54--------55--------56--------57--------58--------59--------60--------61--------62--------63--------64--------65--------66--------67--------68--------69--------70--------71--------72--------73
            LAT            LON  P-LENGHT     SWH_o     SWH_c     P-DIR      LINE     POINT     ROW-Y     COL-X  M-INTENC        dB      WIND INC-ANGLE  ENERGY-M  ENERGY-N  ENERGY-R ENERGY-NN  FG-DEPTH      FLAG         HEADING-DEG    E30-75   E75-390  E390-600 meanincut   meanout   cut/out  STD-NORM  SPEC-MAX   GL-MEAN    GL-VAR      CORR   ENTROPY   HOMOGEN    ENERGY  CONTRAST  DISSIMIL     STD-O    E 1-30 E600-2000   E2000 >      GODA  LONG-HIG     Tmean  CONVOLUT  MEAN-DIR  relation     Sy/Sx  SKEWNESS  KURTOSIS  cutoff-A >50/10000     INT*V   INT-LOG  S-1_g1f1  S-2_g1f2  S-3_g1f3  S-4_g2f1  S-5_g2f2  S-6_g2f3  S-7_g3f1  S-8_g3f2  S-9_g3f3 S-10_g1f4 S-11_g2f4 S-12_g3f4 S-13_g1f5 S-14_g2f5 S-15_g3f5 S-16_g4f1 S-17_g4f2 S-18_g4f3 S-19_g4f4 S-20_g4f5
--------------1--------------2---------3---------4---------5---------6---------7---------8---------9--------10--------11--------12--------13--------14--------15--------16--------17--------18--------19--------20------------------21--------22--------23--------24--------25--------26--------27--------28--------29--------30--------31--------32--------33--------34--------35--------36--------37--------38--------39--------40--------41--------42--------43--------44--------45--------46--------47--------48--------49--------50--------51--------52--------53--------54--------55--------56--------57--------58--------59--------60--------61--------62--------63--------64--------65--------66--------67--------68--------69--------70--------71--------72--------73--------74
     -17.682915     -27.641050     357.2      0.68      1.60      22.5         1         1      1000      1000    468.64   -13.292      11.9      36.6   229.596     0.545      62.9   0.06783     100.0         H               192.2  0.046958  0.013049  0.000486  1.295545  0.669776  1.934296  1.047471      4.97    8.8597   93.0224   0.10011   6.45462   0.14641   0.00315 167.42012   9.03847     490.8  0.483563  0.001105  0.000102  0.000123  0.150897  6.699580  0.548927     86.55  1.000045  1.000003     2.266    11.190      81.3      93.7     202.9     470.2     11.01      1.68     -2.36      1.30     -1.18      0.98      2.27      0.25     -0.26     -0.85      1.10     -0.33     -0.48     -0.01      0.40     -8.30     -3.95      3.65      2.86      0.34
     -17.670082     -27.696405     282.9      0.70      1.60      21.3         1         2      1000      2317    470.98   -13.270      12.2      37.0   230.394     0.551     159.3   0.07132     100.0         H               192.2  0.047531  0.014171  0.000600  1.385492  0.672988  2.058718  1.053859      5.63    8.9015   94.8787   0.10823   6.46105   0.14677   0.00317 169.22086   9.07165     496.3  0.486211  0.002342  0.000430  0.000121  0.153311  6.727176  0.555735     88.07  1.000044  1.000003     2.288    11.328      83.0      93.5     206.6     485.1     11.35      1.65     -2.21      0.58     -1.03      1.11      2.56     -0.16      0.26     -0.79      0.90      0.07      0.31     -0.53      0.53     -9.75     -3.04      2.65      1.78     -0.86
     -17.657347     -27.751272     350.4      0.67      1.60       8.6         1         3      1000      3634    420.97   -13.757      11.7      37.3   231.141     0.549     184.3   0.06836     100.0         H               192.2  0.046150  0.013314  0.000514  1.318306  0.668113  1.973178  1.052723      5.36    7.9178   76.6005   0.10218   6.24526   0.16066   0.00393 137.54828   8.14040     443.2  0.486782  0.002480  0.000511  0.000116  0.153006  6.714252  0.553896     87.06  1.000009  1.000001     2.313    11.895      84.8      92.5     162.6     463.7     11.10      1.68     -2.47      1.01     -1.33      1.36      2.51      0.24     -0.17     -0.77      0.76     -0.24      0.17     -0.53      0.79     -8.77     -3.72      4.11      2.05     -1.26
     -17.735533     -27.654102     399.5      0.68      1.60      20.0         2         1      2444      1000    469.06   -13.288      11.9      36.6   229.785     0.547      86.8   0.06907     100.0         H               192.2  0.047394  0.012913  0.000547  1.298233  0.677885  1.915122  1.049979      5.44    8.8661   93.4184   0.10288   6.45509   0.14674   0.00316 167.61502   9.03728     492.5  0.485279  0.001530  0.000161  0.000122  0.150658  6.681484  0.551971     89.17  1.000026  1.000000     2.289    11.489      81.3      93.7     203.5     491.5     11.21      1.57     -2.33      0.74     -1.12      1.02      2.57      0.00     -0.00     -1.02      1.25     -0.54     -0.28     -0.55      0.62     -9.48     -3.26      3.48      3.34     -0.98
     -17.722694     -27.709475     399.5      0.67      1.60      20.0         2         2      2444      2317    442.35   -13.542      11.8      37.0   229.318     0.544      60.3   0.06817     100.0         H               192.2  0.046597  0.013476  0.000547  1.348077  0.667908  2.018358  1.046931      4.87    8.3393   83.4208   0.10065   6.34220   0.15356   0.00354 150.04866   8.54038     463.1  0.482827  0.001162  0.000087  0.000125  0.152113  6.706124  0.548521     89.25  1.000024  1.000002     2.264    11.226      83.0      93.0     179.3     455.3     11.26      1.69     -2.37      1.01     -0.87      1.34      2.25      0.15     -0.45     -0.69      0.89     -0.58      0.08     -0.76      1.03     -9.47     -3.27      4.96      2.70     -2.12
     -17.709953     -27.764359     276.6      0.68      1.60      32.3         2         3      2444      3634    430.90   -13.656      11.9      37.3   229.811     0.546     101.1   0.06808     100.0         H               192.2  0.046825  0.013588  0.000530  1.370056  0.659403  2.077722  1.049029      4.22    8.1146   79.6682   0.10154   6.29124   0.15736   0.00374 143.15863   8.32439     452.0  0.483876  0.001584  0.000232  0.000127  0.152014  6.718076  0.550625     88.25  1.000048  1.000003     2.281    11.391      83.0      92.7     170.0     448.3     11.14      1.30     -1.95      1.11     -1.02      0.68      2.39     -0.04     -0.04     -1.39      0.92     -0.04     -0.15     -0.54      0.60     -8.72     -2.60      3.00      2.77     -0.98
     -17.788149     -27.667162     331.3      0.69      1.60      20.8         3         1      3888      1000    482.96   -13.161      12.1      36.6   229.580     0.546      65.0   0.06865     100.0         H               192.2  0.046481  0.013379  0.000552  1.326943  0.656155  2.022302  1.048152      5.22    9.1399   98.3883   0.10105   6.51189   0.14321   0.00298 176.89343   9.30450     506.2  0.484245  0.001263  0.000098  0.000121  0.153657  6.714727  0.549748     88.99  1.000025  1.000003     2.263    11.181      81.3      94.0     216.4     487.5     11.24      2.00     -2.80      0.86     -1.27      1.67      2.53      0.04     -0.49     -0.89      1.25     -0.34     -0.33     -0.55      0.87     -9.62     -4.06      5.89      2.89     -1.63
     -17.775305     -27.722551     385.4      0.69      1.60      35.3         3         2      3888      2317    462.46   -13.349      12.1      37.0   230.497     0.549      70.7   0.06970     100.0         H               192.2  0.047943  0.013535  0.000471  1.353884  0.677545  1.998220  1.051661      3.77    8.7340   91.2082   0.10367   6.42683   0.14862   0.00326 163.50565   8.91310     486.4  0.486379  0.001168  0.000141  0.000125  0.149938  6.701683  0.553680     89.50  1.000020  1.000001     2.310    11.722      81.3      93.5     198.3     494.8     10.79      1.46     -2.24      1.79     -0.72      0.88      2.35     -0.02     -0.09     -0.57      0.85     -0.18     -0.39     -0.17      0.45     -7.41     -2.54      3.40      2.22     -0.22
     -17.762558     -27.777454     399.5      0.69      1.60      20.0         3         3      3888      3634    441.00   -13.556      12.0      37.3   229.644     0.547      51.6   0.06899     100.0         H               192.2  0.047846  0.013320  0.000484  1.316922  0.680464  1.935330  1.049257      5.03    8.3144   83.3050   0.10304   6.33634   0.15428   0.00357 149.44197   8.51316     462.7  0.484634  0.001016  0.000061  0.000121  0.153770  6.711741  0.551356     89.24  1.000031  1.000002     2.267    11.163      79.7      93.0     178.7     436.0     11.17      1.97     -2.63      1.23     -1.21      1.21      1.99      0.07      0.04     -0.80      1.06     -0.37     -0.33     -0.69      0.55     -8.06     -3.90      3.91      2.77     -1.05

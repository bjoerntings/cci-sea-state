# CCI Sea State

Open Source repository for DLR's developments within the scope of ESA's CCI+ Sea State ECV project

# 1. GENERAL DESCRIPTION

  REPROCESSOR_S1-WV_SAINT (c++)

  REPROCESSOR_S1-WV_TSVM  (FORTRAN +phyton)
  
  These two independent program-blocks reprocess sea state parameters using pre-processed SAR-features (Sentinel-1 Wave Mode S1-WV). 
  The example data:

  S1-WV_DATA/
  
  Each S1-WV overfligth/product with Scene_ID includes 30-190 individual imagettes - stored in separate DIR: S1-WV_DATA/Scene_ID/. 
  The SAR features are stored for each imagette "S1-WV_DATA/Scene_ID/imagette_ID_results.txt" separated. 
  
  
  The programs re-process sea state parameters for one given Scene_ID
  (e.g: S1A_WV_SLC__1SSV_20171202T023627_20171202T030040_019517_021200_5EC2 with 100 imagettes)
  As result, the sub-dir "cci" will be created in dir "Scene-ID" additional to feature-files, with file "Scene_ID__all-parameters_seastate.txt"

  The results are processed in two steps:

   1.  REPROCESSOR_S1-WV_SAINT - CWVAE_EX - extended CWAEVE algorithm uses linear regression method. 
        As result, the dir "cci" with file "Scene_ID__all-parameters_seastate.txt" created.

   2.  REPROCESSOR_S1-WV_TSVM  - the SWHs are updated using machine learning - Thunder-SVM (SWH estimated using TSVM replaces SWH in output-file)


   This demo-version incudes 15 exampe S1-WV scenes ID for testing the reprocessing sea state parameters. 

# 2.COMPILING

  2.1. REPROCESSOR_S1-WV_SAINT

    cd REPROCESSOR_S1-WV/REPROCESSOR_S1-WV_SAINT/SAINT_ESA

    make

		  
  2.2. REPROCESSOR_S1-WV_TSVM

	cd REPROCESSOR_S1-WV/REPROCESSOR_S1-WV_TSVM/CODE/src/
	./Make.sh

or for each file:		

		gfortran tsvm_processor_1M_main.f90  -o tsvm_processor_1M_main.out
		gfortran tsvm_cut-results_1M_wv1.f90 -o tsvm_cut-results_1M_wv1.out
		gfortran tsvm_cut-results_1M_wv2.f90 -o tsvm_cut-results_1M_wv2.out
		gfortran tsvm_results_map-check.f90  -o tsvm_results_map-check.out
		gfortran TSVM_REPROCESSOR.f90        -o TSVM_REPROCESSOR.out 

# 3. RUN

## 3.1. REPROCESSOR_S1-WV_SAINT   (Linear regression)

Run-dir-location:  REPROCESSOR_S1-WV/REPROCESSOR_S1-WV_SAINT/SAINT_ESA/dist/Debug/GNU-Linux
  
    Run command:  ./saint_esa   INPUT-DIR                  OUTPUT-DIR               Scene-ID

    e.g.: ./saint_esa   ../../../../../S1-WV_DATA ../../../../../S1-WV_DATA  S1A_WV_SLC__1SSV_20171202T023627_20171202T030040_019517_021200_5EC2

Results: results are stored in new created dir "cci"  ../../../../S1-WV_DATA/Scene-ID/cci/Scene-ID._all-parameters_seastate.txt
		
COMMENT: 
  The path for outputs output=input becouse the "input" includes only feture-file(ID_results.txt) but not the sea state resuluts.
  These are processed using linear regression and  complement the product.
  In the next part 3.2. the column "SWH" will be replaced by SWH processed using Thunder Support Vector Machine TSVM.	

## 3.2. REPROCESSOR_S1-WV_TSVM  (Thunder Support Vector Machine TSVM)

Setup:

  -to run python:

      -check the enviroment (*1) 

      -install tsvm-lib (*2)

       -check path to python scripts in file  REPROCESSOR_S1-WV_TSVM/CODE/python/python_lib_path.txt (firt line works and should be "python") 

  -check the input-dir-path in file REPROCESSOR_S1-WV/REPROCESSOR_S1-WV_TSVM/CODE/setting_and_hedings/data_path.txt

   (actual valid path ../../../S1-WV_DATA/)
			   
Run-dir-loacation:  REPROCESSOR_S1-WV/REPROCESSOR_S1-WV_TSVM/CODE/src/
		
    Run-command:   ./TSVM_REPROCESSOR.out Scene-ID

    e.g.: ./TSVM_REPROCESSOR.out S1A_WV_SLC__1SSV_20171202T023627_20171202T030040_019517_021200_5EC2
  
Results:  /REPROCESSOR_S1-WV/REPROCESSOR_S1-WV_TSVM/OUTPUT/

# 4. END PRODUCT

  End-product file "Scene_ID.__all-parameters_seastate.txt" see in dir /REPROCESSOR_S1-WV/REPROCESSOR_S1-WV_TSVM/OUTPUT/
  
=====================================================
## Footnotes 
(*1) python enviroment

  create a new anaconda environment (unix):
  
    command:     conda create --name new_env python=3 scikit-learn

  activate the created environment:

    command:     conda activate new_env
   
(*2)install the thundersvm library:

        command: wget https://github.com/Xtra-Computing/thundersvm/raw/d38af58e0ceb7e5d948f3ef7d2c241ba50133ee6/python/dist/thundersvm-cpu-0.2.0-py3-none-linux_x86_64.whl

        command: pip install thundersvm-cpu-0.2.0-py3-none-linux_x86_64.whl
   



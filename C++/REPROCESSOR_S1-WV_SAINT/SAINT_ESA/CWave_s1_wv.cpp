#include "CWave_s1_wv.h"
#include "xmlParser.h" 
#include <iostream>
#include <fstream>
#include <string>

#define INPUTBUFFER 1000

CWave_s1_wv::CWave_s1_wv()
{
    //new CImg<double>(n_features,n_x+1,n_y+1 ,1,1); // 3D: feature-number, x,y
}

//=============================================================================
// 1. CREATE OUTPUT DIRS AND PATHS
   void CWave_s1_wv::create_outdirs(string input_path,string output_path,string scene_id)
//=============================================================================
{
	data_dir    = string(input_path);
	out_dir     = string(output_path);
	product_id  = string(scene_id); 

	unsigned int buffer = 600;
	char command[buffer];

	snprintf(command,buffer, "%s%s",      "mkdir -p ", out_dir.c_str()                               ); system(command);
	snprintf(command,buffer, "%s%s%s%s",  "mkdir -p ", out_dir.c_str(), "/",product_id.c_str()       ); system(command);
	snprintf(command,buffer, "%s%s%s%s%s","mkdir -p ", out_dir.c_str(), "/",product_id.c_str(),"/cci"); system(command);   

	snprintf(command,buffer, "%s%s%s%s%s","rm -f "   , out_dir.c_str(), "/",product_id.c_str(),"/cci/*txt" ); system(command);       

}
//=============================================================================
// 1. READ DATA FEATURES
void CWave_s1_wv::read_input_data(cimg_library::CImg<double>* sar_features_matrix, string imagette)
//=============================================================================
{
//-----------------------------------------------------------------------------
// 1.1 Open feature file
//-----------------------------------------------------------------------------
	imagette_id = string(imagette);

	unsigned int buffersize = 500;

	char char1[buffersize]; 
	char char2[buffersize];

	snprintf(char1, buffersize,"%s%s%s%s%s",data_dir.c_str(),"/",product_id.c_str(),"/",imagette_id.c_str());
	snprintf(char2, buffersize,"%s%s%s",out_dir.c_str(),"/",product_id.c_str());


	feature_file =string(char1);

	cout <<"input imagette: "<<imagette_id  <<endl;

      // cout <<"read_input_data: feature_file = "<<feature_file <<endl;
      // cout <<"output_path = "<<output_path  <<endl;

//----------------------------------------------------------------------------
//   1.2 Accounting lines in a feature-file
//----------------------------------------------------------------------------	 	 
		 char LINE[INPUTBUFFER];	 LINES_NUMBER =0;
		
		ifstream  feature_file_to_read_lines; 
		feature_file_to_read_lines.open (feature_file.c_str()); 
		while(feature_file_to_read_lines.getline(LINE, INPUTBUFFER))
		{ 
			  LINES_NUMBER = LINES_NUMBER +1; 
			  // cout<<" LINE_NUMBER = "<<LINES_NUMBER<<endl;   
			  if(feature_file_to_read_lines.eof() ) break;
			
		}
			
			LINES_NUMBER = LINES_NUMBER - 22;  // 22=header 
			
		feature_file_to_read_lines.close();	
			
		//cout<<"  LINES: "<<LINES_NUMBER <<endl;   
//-----------------------------------------------------------------------------
//  1.3 Read features 	
//-----------------------------------------------------------------------------		
		ifstream feature_file_to_read; 
		feature_file_to_read.open (feature_file.c_str());

		float corners_coordinates[4][2];
		char dummy[INPUTBUFFER]; int y,x; char dum_1[1];

		seastate_key = vector<char>(100);
	 
		float FEATURES_1[100][19] ;  //  0-18 : 19 Elemente
		float FEATURES_2[100][54] ;  // 20-73 : 54 Elemente
		float FEATURES[100][74]   ;  //  0-73 : 74 Elemente (19 + 1(key) + 54  
	
		//read line 1 (dummy)
		feature_file_to_read.getline(dummy, INPUTBUFFER);
			// cout<<"READ 1:" << dummy<<endl;
			 
		//read line 2-5 (corber geo-coordinates)	 
			for(y=0; y<4; y++){
			for(x=0; x<2; x++){ 
                feature_file_to_read >> corners_coordinates[y][x];
			}}

		//cout<<"CORNER 1:  LAT="<<corners_coordinates[0][0]<<"  LON="<<corners_coordinates[0][1]<<endl; 
		//cout<<"CORNER 2:  LAT="<<corners_coordinates[1][0]<<"  LON="<<corners_coordinates[1][1]<<endl; 
		//cout<<"CORNER 3:  LAT="<<corners_coordinates[2][0]<<"  LON="<<corners_coordinates[2][1]<<endl; 
 		//cout<<"CORNER 4:  LAT="<<corners_coordinates[3][0]<<"  LON="<<corners_coordinates[3][1]<<endl; 

		//read rest of line 5
			 feature_file_to_read.getline(dummy, INPUTBUFFER);
			 
			 
		//read dummy lines  	 
			for(y=0; y<17; y++){
			feature_file_to_read.getline(dummy, INPUTBUFFER);
					// cout<<"READ LINE:"<<y+6<<"  "<< dummy<<endl;
			}

		//read features: matrix-1 before KEY-column, KEY(column), matrix-2 after KEY  	 2021-10-08
			for(y=0; y < LINES_NUMBER; y++){
			 	for(x=0; x < 19; x++){                          // read first 18 featueres int/float
				feature_file_to_read >> FEATURES_1[y][x];
					// cout<<"LINE:"<<y<<" FEATURE:"<<x<<"  VALUE="<<FEATURES_1[y][x]<<endl;
				}
			              
				feature_file_to_read >> seastate_key[y];   // read feature=19 - char (key)
						//  cout<<"LINE:"<<y<<" FEATURE: 19 KEY ="<<seastate_key[y]<<endl;
							 
				for(x=0; x<54; x++){
				feature_file_to_read >> FEATURES_2[y][x];// read last 54 featueres int/float   53=73 
				        // cout<<"LINE: "<<y+1<<" FEATURE: "<<x+20<<"  VALUE="<<FEATURES_2[y][x]<<endl;
				}
			}


		// save uniform features-matirix from 3 matix  (matrix-1 before KEY-column, KEY(column), matrix-2 after KEY  	 )	
			for(y=0; y < LINES_NUMBER;  y++){
			for(x=0; x < 74; x++)           {
				if(x < 19)FEATURES[y][x]=FEATURES_1[y][x];
				if(x ==19)FEATURES[y][x]=0;
				if(x > 19)FEATURES[y][x]=FEATURES_2[y][x-20];
				//if(x==73) cout<<"FEATURE: "<<x<<"   ==== FEATURES[][]="<<FEATURES[y][x]<<"  line="<<y+1<< endl;
			}}

		feature_file_to_read.getline(dummy, INPUTBUFFER);

		feature_file_to_read.close();
//-----------------------------------------------------------------------------
// 1.4 Features Zuordnung/assignment 
//-----------------------------------------------------------------------------

		int file_lines_n= LINES_NUMBER; int n_p_features=54; int check_land=0;

	
		sar_metadata_matrix = vector<vector <float> > (4);                  // 4   features/metadata 
	
		sar_metadata_matrix[0] = vector <float> (file_lines_n);
		sar_metadata_matrix[1] = vector <float> (file_lines_n);
		sar_metadata_matrix[2] = vector <float> (file_lines_n);
		sar_metadata_matrix[3] = vector <float> (file_lines_n);
	
		Incut        = vector <float> (file_lines_n);
		HeadingAngle = vector <float> (file_lines_n);
	
		for(unsigned int isl=0; isl< file_lines_n; isl++)     
		{  

		(*sar_features_matrix)(0,  isl)=1;
		(*sar_features_matrix)(1,  isl)=FEATURES[isl][10]; //MEAN-INT 
		(*sar_features_matrix)(2,  isl)=FEATURES[isl][12]; //WIND 
		(*sar_features_matrix)(3,  isl)=FEATURES[isl][15]; //ENERGY_N 
		(*sar_features_matrix)(4,  isl)=FEATURES[isl][16]; //ENERGY_R  
		(*sar_features_matrix)(5,  isl)=FEATURES[isl][17]; //ENERGY_NN 
		(*sar_features_matrix)(6,  isl)=FEATURES[isl][21]; //E_75 
		(*sar_features_matrix)(7,  isl)=FEATURES[isl][22]; //E_390  
		(*sar_features_matrix)(8,  isl)=FEATURES[isl][23]; //E_600 
		(*sar_features_matrix)(9,  isl)=FEATURES[isl][27]; //STD_NORM 
		(*sar_features_matrix)(10, isl)=FEATURES[isl][28]; //SPEC_MAX 
		
		(*sar_features_matrix)(11, isl)=FEATURES[isl][29]; //GLCM-MEAN
		(*sar_features_matrix)(12, isl)=FEATURES[isl][30]; //GLCM-VARIANCE
		(*sar_features_matrix)(13, isl)=FEATURES[isl][31]; //GLCM-CORR
		(*sar_features_matrix)(14, isl)=FEATURES[isl][32]; //GLCM-ENTRO
		(*sar_features_matrix)(15, isl)=FEATURES[isl][33]; //GLCM-HOMO
		(*sar_features_matrix)(16, isl)=FEATURES[isl][34]; //GLCM-ENERGY
		(*sar_features_matrix)(17, isl)=FEATURES[isl][35]; //GLCM-CONTRAST
		(*sar_features_matrix)(18, isl)=FEATURES[isl][36]; //GLCM-DISS
		(*sar_features_matrix)(19, isl)=FEATURES[isl][37]; //STD-O
		(*sar_features_matrix)(20, isl)=FEATURES[isl][38]; //E_30
		
		(*sar_features_matrix)(21, isl)=FEATURES[isl][39]; //E_2000
		(*sar_features_matrix)(22, isl)=FEATURES[isl][40]; //E_2000>
		(*sar_features_matrix)(23, isl)=FEATURES[isl][41]; //GODA
		(*sar_features_matrix)(24, isl)=FEATURES[isl][42]; //LONG_HIG
		(*sar_features_matrix)(25, isl)=FEATURES[isl][44]; //CONVOLUTION
		(*sar_features_matrix)(26, isl)=FEATURES[isl][46]; //RELATION
		(*sar_features_matrix)(27, isl)=FEATURES[isl][47]; //SytoSx
		(*sar_features_matrix)(28, isl)=FEATURES[isl][48]; //SKEW
		(*sar_features_matrix)(29, isl)=FEATURES[isl][49]; //KURT
		(*sar_features_matrix)(30, isl)=FEATURES[isl][50]; //CUTOFF	

		(*sar_features_matrix)(31, isl)=FEATURES[isl][51]; //>50to10000		
		(*sar_features_matrix)(32, isl)=FEATURES[isl][52]; //INT*V		
		(*sar_features_matrix)(33, isl)=FEATURES[isl][53]; //INT-LOG	
		
		(*sar_features_matrix)(34, isl)=FEATURES[isl][54]; //S-1	
		(*sar_features_matrix)(35, isl)=FEATURES[isl][55]; //S-2	
		(*sar_features_matrix)(36, isl)=FEATURES[isl][56]; //S-3	
		(*sar_features_matrix)(37, isl)=FEATURES[isl][57]; //S-4	
		(*sar_features_matrix)(38, isl)=FEATURES[isl][58]; //S-5	
		(*sar_features_matrix)(39, isl)=FEATURES[isl][59]; //S-6	
		(*sar_features_matrix)(40, isl)=FEATURES[isl][60]; //S-7	

		(*sar_features_matrix)(41, isl)=FEATURES[isl][61]; //S-8
		(*sar_features_matrix)(42, isl)=FEATURES[isl][62]; //S-9
		(*sar_features_matrix)(43, isl)=FEATURES[isl][63]; //S-10
		(*sar_features_matrix)(44, isl)=FEATURES[isl][64]; //S-11
		(*sar_features_matrix)(45, isl)=FEATURES[isl][65]; //S-12
		(*sar_features_matrix)(46, isl)=FEATURES[isl][66]; //S-13
		(*sar_features_matrix)(47, isl)=FEATURES[isl][67]; //S-14
		(*sar_features_matrix)(48, isl)=FEATURES[isl][68]; //S-15
		(*sar_features_matrix)(49, isl)=FEATURES[isl][69]; //S-16
		(*sar_features_matrix)(50, isl)=FEATURES[isl][70]; //S-17

		(*sar_features_matrix)(51, isl)=FEATURES[isl][71]; //S-18
		(*sar_features_matrix)(52, isl)=FEATURES[isl][72]; //S-19
		(*sar_features_matrix)(53, isl)=FEATURES[isl][73]; //S-20

		Incut[isl]                     =FEATURES[isl][24]; //meanincut additional
		HeadingAngle[isl]              =FEATURES[isl][20]; //heading	 additional

		// cout<<"FEATURE 53:   "<<(*sar_features_matrix)(53,isl)<<"       LINE="<<isl+1<<endl;

// Metadata: 0=incedence-angle, 1=lat, 2=lon, 3=LANDMASK  

		sar_metadata_matrix[0][isl]=FEATURES[isl][13]; //incedence-angle
		sar_metadata_matrix[1][isl]=FEATURES[isl][0];  //LAT 
		sar_metadata_matrix[2][isl]=FEATURES[isl][1];  //LON
		sar_metadata_matrix[3][isl]=0 ;                //LAND=1  WET=0


		if(seastate_key[isl] =='I')sar_metadata_matrix[3][isl] = 1 ;  //LAND   KEY =I (ISLANDS)

	/*  
 		cout<<"incedence-angle  MEADATA 0:"<< sar_metadata_matrix[0][isl] <<"   isl="<<isl+1<<endl;
		cout<<"LAT              MEADATA 1:"<< sar_metadata_matrix[1][isl] <<"   isl="<<isl+1<<endl;
		cout<<"LON              MEADATA 2:"<< sar_metadata_matrix[2][isl] <<"   isl="<<isl+1<<endl;
		cout<<"LAND             MEADATA 3:"<< sar_metadata_matrix[3][isl] <<"   isl="<<isl+1<<endl;
	*/
	
		}

}
//================================================================================================  
// 2. Read coeffitients 
void CWave_s1_wv::read_coeffitiens()
//================================================================================================   
{
	string tmpConfFile = "../../../settings/S1-WV_coeffitients_cwave.xml";    // 22 s1  wv

	if(exists(tmpConfFile))
	{     
				XMLNode xMainNode;
				xMainNode=XMLNode::openFileHelper(tmpConfFile.c_str(),"ReadCoeffitinentsS1wv");
//----------------------------------------------------------------------------------------
	n_sar_features = atoi(xMainNode.getChildNode("n_sar_features").getText());
				
				// cout<<"READ COEFFITIENTS: "<<n_sar_features<<endl;
//=================================================================================================
//   SWH   read coeffitients / mean /std 
//=================================================================================================	
//   swh wv1 (significant wave height) 		/VV 

				  XMLNode xNode=xMainNode.getChildNode("coefftients_swh_vector_1"	);	n_sar_features = atoi(xNode.getAttributeValue(0));

							std::vector<std::string> coefftients_swh_vector_1_string	= splitString(xNode.getText());
													 coefftients_swh_vector_1			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)       {coefftients_swh_vector_1[lidx]	= atof(coefftients_swh_vector_1_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_swh_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_swh_vector_1_string			= splitString(xNode.getText());
			                                          mean_swh_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)       {mean_swh_vector_1[lidx]			= atof(mean_swh_vector_1_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_swh_vector_1");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_swh_vector_1_string			= splitString(xNode.getText());
													  std_swh_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_swh_vector_1[lidx]			= atof(std_swh_vector_1_string[lidx].c_str());			}  
//=================================================================================================	
//  swh wv2 
						xNode=xMainNode.getChildNode("coefftients_swh_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_swh_vector_2_string	= splitString(xNode.getText());
													  coefftients_swh_vector_2			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_swh_vector_2[lidx]	= atof(coefftients_swh_vector_2_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_swh_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_swh_vector_2_string			= splitString(xNode.getText());
													  mean_swh_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_swh_vector_2[lidx]			= atof(mean_swh_vector_2_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_swh_vector_2");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_swh_vector_2_string			= splitString(xNode.getText());
													  std_swh_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_swh_vector_2[lidx]			= atof(std_swh_vector_2_string[lidx].c_str());			} 

// cout<<"READ COEFFITIENTS SWH OK: "<<n_sar_features<<endl;
//=================================================================================================
//   Tm0  mean period   
//=================================================================================================	
//   tm0 wv1  (mean period) 		
						xNode=xMainNode.getChildNode("coefftients_tm0_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_tm0_vector_1_string	= splitString(xNode.getText());
													  coefftients_tm0_vector_1			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_tm0_vector_1[lidx]	= atof(coefftients_tm0_vector_1_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_tm0_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_tm0_vector_1_string			= splitString(xNode.getText());
													  mean_tm0_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)      {mean_tm0_vector_1[lidx]			= atof(mean_tm0_vector_1_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_tm0_vector_1");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_tm0_vector_1_string			= splitString(xNode.getText());
													  std_tm0_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_tm0_vector_1[lidx]			= atof(std_tm0_vector_1_string[lidx].c_str());			}  
//=================================================================================================	
//   tm0 wv2  (mean period) 	

						xNode=xMainNode.getChildNode("coefftients_tm0_vector_2");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_tm0_vector_2_string	= splitString(xNode.getText());
													  coefftients_tm0_vector_2			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_tm0_vector_2[lidx]	= atof(coefftients_tm0_vector_2_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_tm0_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_tm0_vector_2_string			= splitString(xNode.getText());
													  mean_tm0_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_tm0_vector_2[lidx]			= atof(mean_tm0_vector_2_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_tm0_vector_2");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_tm0_vector_2_string			= splitString(xNode.getText());
													  std_tm0_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_tm0_vector_2[lidx]			= atof(std_tm0_vector_2_string[lidx].c_str());			}  

//=================================================================================================
//   Tm1  period   
//=================================================================================================	
//   tm1 wv1  ( period) 		
						xNode=xMainNode.getChildNode("coefftients_tm1_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_tm1_vector_1_string	= splitString(xNode.getText());
													  coefftients_tm1_vector_1			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_tm1_vector_1[lidx]	= atof(coefftients_tm1_vector_1_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_tm1_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_tm1_vector_1_string			= splitString(xNode.getText());
													  mean_tm1_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_tm1_vector_1[lidx]			= atof(mean_tm1_vector_1_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_tm1_vector_1");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_tm1_vector_1_string			= splitString(xNode.getText());
													  std_tm1_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_tm1_vector_1[lidx]			= atof(std_tm1_vector_1_string[lidx].c_str());			}  
//=================================================================================================	
//   tm1 wv2  ( period) 
		
						xNode=xMainNode.getChildNode("coefftients_tm1_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_tm1_vector_2_string	= splitString(xNode.getText());
													  coefftients_tm1_vector_2			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_tm1_vector_2[lidx]	= atof(coefftients_tm1_vector_2_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_tm1_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_tm1_vector_2_string			= splitString(xNode.getText());
													  mean_tm1_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_tm1_vector_2[lidx]			= atof(mean_tm1_vector_2_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_tm1_vector_2");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_tm1_vector_2_string			= splitString(xNode.getText());
													  std_tm1_vector_2					= vector<double>(n_sar_features);       
   for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_tm1_vector_2[lidx]			= atof(std_tm1_vector_2_string[lidx].c_str());			}  

//=================================================================================================
//   Tm2   period   
//=================================================================================================	
//   tm2 wv1  ( period) 		
						xNode=xMainNode.getChildNode("coefftients_tm2_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_tm2_vector_1_string	= splitString(xNode.getText());
													  coefftients_tm2_vector_1			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_tm2_vector_1[lidx]	= atof(coefftients_tm2_vector_1_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_tm2_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_tm2_vector_1_string			= splitString(xNode.getText());
													  mean_tm2_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)       {mean_tm2_vector_1[lidx]			= atof(mean_tm2_vector_1_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_tm2_vector_1");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_tm2_vector_1_string			= splitString(xNode.getText());
													  std_tm2_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_tm2_vector_1[lidx]			= atof(std_tm2_vector_1_string[lidx].c_str());			}  
//=================================================================================================	
//   tm0 wv2  (mean period) 	
	
						xNode=xMainNode.getChildNode("coefftients_tm2_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_tm2_vector_2_string	= splitString(xNode.getText());
													  coefftients_tm2_vector_2			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_tm2_vector_2[lidx]	= atof(coefftients_tm2_vector_2_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
	                    xNode=xMainNode.getChildNode("mean_tm2_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_tm2_vector_2_string			= splitString(xNode.getText());
													  mean_tm2_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_tm2_vector_2[lidx]			= atof(mean_tm2_vector_2_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_tm2_vector_2");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_tm2_vector_2_string			= splitString(xNode.getText());
													  std_tm2_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_tm2_vector_2[lidx]			= atof(std_tm2_vector_2_string[lidx].c_str());			}  

//=================================================================================================
//   swell-1 sw1      
//=================================================================================================	
//   sw1 wv1  (swell-1) 		
						xNode=xMainNode.getChildNode("coefftients_sw1_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_sw1_vector_1_string	= splitString(xNode.getText());
													  coefftients_sw1_vector_1			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_sw1_vector_1[lidx]	= atof(coefftients_sw1_vector_1_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_sw1_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_sw1_vector_1_string			= splitString(xNode.getText());
													  mean_sw1_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_sw1_vector_1[lidx]			= atof(mean_sw1_vector_1_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_sw1_vector_1");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_sw1_vector_1_string			= splitString(xNode.getText());
													  std_sw1_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_sw1_vector_1[lidx]			= atof(std_sw1_vector_1_string[lidx].c_str());			}  
//=================================================================================================	
//   sw1 wv2  (swell-1) 
 		
				        xNode=xMainNode.getChildNode("coefftients_sw1_vector_2");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_sw1_vector_2_string	= splitString(xNode.getText());
													  coefftients_sw1_vector_2			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_sw1_vector_2[lidx]	= atof(coefftients_sw1_vector_2_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_sw1_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_sw1_vector_2_string			= splitString(xNode.getText());
													  mean_sw1_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_sw1_vector_2[lidx]			= atof(mean_sw1_vector_2_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_sw1_vector_2");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_sw1_vector_2_string			= splitString(xNode.getText());
													  std_sw1_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_sw1_vector_2[lidx]			= atof(std_sw1_vector_2_string[lidx].c_str());			} 

//=================================================================================================
//   swell-2 sw2      
//=================================================================================================	
//   sw2 wv1  (swell-2) 		
						xNode=xMainNode.getChildNode("coefftients_sw2_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_sw2_vector_1_string	= splitString(xNode.getText());
													  coefftients_sw2_vector_1			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_sw2_vector_1[lidx]	= atof(coefftients_sw2_vector_1_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_sw2_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_sw2_vector_1_string			= splitString(xNode.getText());
													  mean_sw2_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_sw2_vector_1[lidx]			= atof(mean_sw2_vector_1_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_sw2_vector_1");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_sw2_vector_1_string			= splitString(xNode.getText());
													  std_sw2_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_sw2_vector_1[lidx]			= atof(std_sw2_vector_1_string[lidx].c_str());			}  
//=================================================================================================	
//   sw2 wv2  ( swell-2) 
		
						xNode=xMainNode.getChildNode("coefftients_sw2_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_sw2_vector_2_string	= splitString(xNode.getText());
													  coefftients_sw2_vector_2			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_sw2_vector_2[lidx]  = atof(coefftients_sw2_vector_2_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_sw2_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_sw2_vector_2_string			= splitString(xNode.getText());
													  mean_sw2_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_sw2_vector_2[lidx]			= atof(mean_sw2_vector_2_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_sw2_vector_2");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_sw2_vector_2_string			= splitString(xNode.getText());
													  std_sw2_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_sw2_vector_2[lidx]			= atof(std_sw2_vector_2_string[lidx].c_str());			}   
 
//=================================================================================================
//   windsea wave height sww      
//=================================================================================================	
//   sww wv1  (windsea) 		
						xNode=xMainNode.getChildNode("coefftients_sww_vector_1");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_sww_vector_1_string = splitString(xNode.getText());
													  coefftients_sww_vector_1        = vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_sww_vector_1[lidx]  = atof(coefftients_sww_vector_1_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_sww_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_sww_vector_1_string			= splitString(xNode.getText());
													  mean_sww_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_sww_vector_1[lidx]			= atof(mean_sww_vector_1_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_sww_vector_1");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_sww_vector_1_string			= splitString(xNode.getText());
													  std_sww_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_sww_vector_1[lidx]			= atof(std_sww_vector_1_string[lidx].c_str());			}  
//=================================================================================================	
//   sww wv2  ( windsea) 	
 
						xNode=xMainNode.getChildNode("coefftients_sww_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_sww_vector_2_string	= splitString(xNode.getText());
													  coefftients_sww_vector_2			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_sww_vector_2[lidx]	= atof(coefftients_sww_vector_2_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_sww_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_sww_vector_2_string			= splitString(xNode.getText());
													  mean_sww_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_sww_vector_2[lidx]			= atof(mean_sww_vector_2_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_sww_vector_2");n_sar_features	= atoi(xNode.getAttributeValue(0));
							std::vector<std::string>  std_sww_vector_2_string			= splitString(xNode.getText());
													  std_sww_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_sww_vector_2[lidx]			= atof(std_sww_vector_2_string[lidx].c_str());			}

//=================================================================================================
//   windsea period tmw      
//=================================================================================================	
//   tmw wv1  (windsea period) 		
						xNode=xMainNode.getChildNode("coefftients_tmw_vector_1");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_tmw_vector_1_string	= splitString(xNode.getText());
			 										  coefftients_tmw_vector_1			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_tmw_vector_1[lidx]	= atof(coefftients_tmw_vector_1_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_tmw_vector_1");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_tmw_vector_1_string			= splitString(xNode.getText());
													  mean_tmw_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)      {mean_tmw_vector_1[lidx]			= atof(mean_tmw_vector_1_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_tmw_vector_1");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_tmw_vector_1_string			= splitString(xNode.getText());
													  std_tmw_vector_1					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {std_tmw_vector_1[lidx]			= atof(std_tmw_vector_1_string[lidx].c_str());			}  
//=================================================================================================	
//   tmw wv2  (windsea period) 	
		
						xNode=xMainNode.getChildNode("coefftients_tmw_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  coefftients_tmw_vector_2_string	= splitString(xNode.getText());
													  coefftients_tmw_vector_2			= vector<double>(n_sar_features);   
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {coefftients_tmw_vector_2[lidx]	= atof(coefftients_tmw_vector_2_string[lidx].c_str());	}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("mean_tmw_vector_2");n_sar_features= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  mean_tmw_vector_2_string			= splitString(xNode.getText());
													  mean_tmw_vector_2					= vector<double>(n_sar_features);       
	for(int lidx=0;lidx<n_sar_features; lidx++)		 {mean_tmw_vector_2[lidx]			= atof(mean_tmw_vector_2_string[lidx].c_str());			}
//----------------------------------------------------------------------------------------
						xNode=xMainNode.getChildNode("std_tmw_vector_2");n_sar_features	= atoi(xNode.getAttributeValue(0));
			 				std::vector<std::string>  std_tmw_vector_2_string			= splitString(xNode.getText());
													  std_tmw_vector_2					= vector<double>(n_sar_features);       
   for(int lidx=0;lidx<n_sar_features; lidx++) 		 {std_tmw_vector_2[lidx]			= atof(std_tmw_vector_2_string[lidx].c_str());			}
 

//----------------------------------------------------------------------------------------   
	}  
	else if(verbose_level>=1)   
	{
		cout << tmpConfFile << " could not be found or opened! Using default input." << endl;
	}			 

}
//=============================  S1 WV ==============================================================
// 3. SAR-FEATURES FORM SUBCENES AVAREGING TO ONE VALUE FOR 1 IMAGETTE 
   void CWave_s1_wv::calulate_seastate_parameters(cimg_library::CImg<double>* sar_features_matrix)	
//===================================================================================================
{
//---------------------------------------------------------------------------------------------------
// 3.1. Definition
//--------------------------------------------------------------------------------------------------- 
        int n_p_features = 54 ; // input features - first-order-features (no combinations), total 131  with combinations (matrix: sar_features_mean )
	    int n_features   = 131; // all features (mean) 
		
		int n_metadata   = 4;	// metadata: 0=incedence-angle, 1=lat, 2=lon, 3=LANDMASK  
		int n_parameters = 8;   // 0=SWH, 1=TM0, 2=TM1, 3=TM2, 4=SW1, 5=SW2, 6=SWW, 7=TWW 

		sar_features_mean = vector<double>(n_features);                 // 131 features 
		sar_metadata_mean = vector<float>(n_metadata);                  // 4   features 
		results           = vector<float>(n_metadata + n_parameters);   // metadata&sea-state-parameters
		
//---------------------------------------------------------------------------------------------------		
//  3.2. Mean value over imagette: sar features (will be used for multiplication coeffitients) -----		
//---------------------------------------------------------------------------------------------------		
	for (int isf=0; isf < n_p_features;  isf++)     
	{ // do-loop over first-order features XXX
		sar_features_mean[isf] = 0;
		// cout<<"isf="<<isf<<" FEATURE"<<endl;
		if (isf == 0 ) sar_features_mean[isf]=1.0;  // constante,  feature=1   
		if (isf >  0 ) 
		{ // features >1 
		 
			 double value =0; int  zahler=0;    
  			for(unsigned int isl=0; isl< LINES_NUMBER; ++isl)	
			{    // do-loop-subscenes 
		 
			// cout<<  "    FEATURES TO BE AVEARGED:"<< (*sar_features_matrix)(isf,isl)<<endl;
				
			 int key=0;
				if ((*sar_features_matrix)(1,isl) < 0.01) {key=1;land_key=1;}   // land raus - intencity == 0!
				if ((*sar_features_matrix)(1,isl) < 0.01) {key=1;           }   // land raus - intencity == 0!
		 		if ((*sar_features_matrix)(9,isl) > 2.0   )key=1;               // std_n
				if ((*sar_features_matrix)(4,isl) > 5000.0)key=1;               // E_r
			 
				// if(key > 0)cout<< "key="<<key<<" F1= "<<(*sar_features_matrix)(1,isl)<<" F9= "<<(*sar_features_matrix)(9,isl)<<" F4= "<< (*sar_features_matrix)(4,isl)<<"x-y= "<<isl<< endl; 
			 
				if (key == 0 ) 
				{                        
					value = value + (*sar_features_matrix)(isf,isl) ;
					zahler= zahler +1;
				// cout<<" ZAHLER="<<zahler<< endl; 
				// cout<<"AVERAGING INCOMING: FEATURE N= "<<isf<<" feature= "<<(*sar_features_matrix)(isf,isl)<<"x-y= "<<isl<<" key="<<key<< endl;
				}
 			} // do-loop-subscenes end

			if(zahler >  0)  sar_features_mean[isf] =  value/zahler;  // 1D vector
			if(zahler == 0) {sar_features_mean[isf] =  1; }        
			 

		} // features >1 
	}// do-loop over features XXX END
	
	
		int zahler=0; MeanHeadingAngle=0; Eaverageincut=0;
		for(unsigned int isl=0; isl< LINES_NUMBER; ++isl)
		{    // do-loop-subscenes
			if((*sar_features_matrix)(1,isl) > 1 )
			{
				MeanHeadingAngle = MeanHeadingAngle+ HeadingAngle[isl];
				Eaverageincut    = Eaverageincut+Incut[isl]; 
				zahler= zahler +1;
			}
		}

		if(zahler > 0) Eaverageincut    =  Eaverageincut   /zahler; 
		if(zahler > 0) MeanHeadingAngle =  MeanHeadingAngle/zahler; 

			// cout<<"FEATURE  N=106 ZAHLER="<<zahler<<" mean feature="<<Eaverageincut<<endl;

		if(zahler               == 0){ Eaverageincut=1; MeanHeadingAngle = 1;}
		if(sar_features_mean[1] == 1){ Eaverageincut=1; MeanHeadingAngle = 1;}
	
			//	cout<<"***  ALL FIRST-ORDER-FEATURES AVERAGED ***"<< endl; 
				   
//---------------------------------------------------------------------------------------------------	
//3.3. Feature combinations  for S1 WV from mean values 	
//---------------------------------------------------------------------------------------------------             
			sar_features_mean[54]  =      sar_features_mean[6] / sar_features_mean[7] ; /// 1  E_75/E_390 
			sar_features_mean[55]  = 1./( sar_features_mean[2]   +0.1)         ; /// 2  1/WIND
			sar_features_mean[56]  = 1./  sar_features_mean[3]                 ; /// 3  1/ENERGY-N
			sar_features_mean[57]  = 1./  sar_features_mean[6]                 ; /// 4  1/E_75
			sar_features_mean[58]  = 1./  sar_features_mean[7]                 ; /// 5  1/E_390
			sar_features_mean[59]  = 1./  sar_features_mean[8]                 ; /// 6  1/E_600
			sar_features_mean[60]  = 1./  sar_features_mean[9]                 ; /// 7  1/STDn
			 
			sar_features_mean[61]  = 1./ (sar_features_mean[11] + 0.10)        ; /// 8  1/GLCM_mean
			sar_features_mean[62]  = 1./ (sar_features_mean[12] + 1.00)        ; /// 9  1/GLCM_var
			sar_features_mean[63]  = 1./ (sar_features_mean[13] + 0.01)        ; /// 10 1/GLCM_cor
			sar_features_mean[64]  = 1./ (sar_features_mean[14] + 0.10)        ; /// 11 1/GLCM_entr
			sar_features_mean[65]  = 1./  sar_features_mean[15]                ; /// 12 1/GLCM_homo
			sar_features_mean[66]  = 1./  sar_features_mean[16]                ; /// 13 1/GLCM_energ
			sar_features_mean[67]  = 1./ (sar_features_mean[17] + 0.1)         ; /// 14 1/GLCM_contr
			sar_features_mean[68]  = 1./ (sar_features_mean[18] + 0.1)         ; /// 15 1/GLCM_diss
			sar_features_mean[69]  = 1./  sar_features_mean[19]                ; /// 16 1/STDo
			sar_features_mean[70]  = 1./  sar_features_mean[20]     		      ; /// 17 1/E_30

			sar_features_mean[71]  = 1./ (sar_features_mean[21]+0.0005)        ; /// 18 1/E_2000
			sar_features_mean[72]  = 1./  sar_features_mean[23]                ; /// 19 1/GODA
			sar_features_mean[73]  = 1./  sar_features_mean[24]                ; /// 20 1/LONGHIGG
			sar_features_mean[74]  = 1./  sar_features_mean[25]                ; /// 21 1/CONVOLUT
			sar_features_mean[75]  = 1./  sar_features_mean[28]                ; /// 22 1/SKEW
			sar_features_mean[76]  = 1./  sar_features_mean[29]                ; /// 23 1/KURT
			sar_features_mean[77]  = 1./  sar_features_mean[30]                ; /// 24 1/CUTOFF
			sar_features_mean[78]  = 1./ (sar_features_mean[31]+1.)            ; /// 25 1/>50to10000
			sar_features_mean[79]  = 1./ (sar_features_mean[32]+1.)            ; /// 26 1/int*V
			sar_features_mean[80]  = 1./ (sar_features_mean[33]+1.)            ; /// 27 1/int_log

			sar_features_mean[81]  = 1./  sar_features_mean[34]                ; /// 28 1/S-1
			sar_features_mean[82]  = 1./ (sar_features_mean[35]+100.)          ; /// 39 1/S-2
			sar_features_mean[83]  = 1./ (sar_features_mean[36]+100.)          ; /// 30 1/S-3
			sar_features_mean[84]  = 1./ (sar_features_mean[37]+100.)          ; /// 31 1/S-4
			sar_features_mean[85]  = 1./ (sar_features_mean[38]+100.)          ; /// 32 1/S-5
			sar_features_mean[86]  = 1./ (sar_features_mean[39]+100.)          ; /// 33 1/S-6
			sar_features_mean[87]  = 1./ (sar_features_mean[40]+100.)          ; /// 34 1/S-7
			sar_features_mean[88]  = 1./ (sar_features_mean[41]+100.)          ; /// 35 1/S-8 
			sar_features_mean[89]  = 1./ (sar_features_mean[42]+100.)          ; /// 36 1/S-9 
			sar_features_mean[90]  = 1./ (sar_features_mean[43]+100.)          ; /// 37 1/S-10 

			sar_features_mean[91]  = 1./ (sar_features_mean[44]+100.)          ; /// 38 1/S-11 
			sar_features_mean[92]  = 1./ (sar_features_mean[45]+100.)          ; /// 39 1/S-12
			sar_features_mean[93]  = 1./ (sar_features_mean[46]+100.)          ; /// 40 1/S-13
			sar_features_mean[94]  = 1./ (sar_features_mean[47]+100.)          ; /// 41 1/S-14
			sar_features_mean[95]  = 1./ (sar_features_mean[48]+100.)          ; /// 42 1/S-15
			sar_features_mean[96]  = 1./ (sar_features_mean[49]+100.)          ; /// 43 1/S-16
			sar_features_mean[97]  = 1./ (sar_features_mean[50]+100.)          ; /// 44 1/S-17
			sar_features_mean[98]  = 1./ (sar_features_mean[51]+100.)          ; /// 45 1/S-18
			sar_features_mean[99]  = 1./ (sar_features_mean[52]+100.)          ; /// 46 1/S-19
			sar_features_mean[100] = 1./ (sar_features_mean[53]+100.)          ; /// 47 1/S-20		 
			 
			sar_features_mean[101] =      sar_features_mean[9 ] * sar_features_mean[9 ]            ; /// 48 STDn**2
			sar_features_mean[102] =      sar_features_mean[19] * sar_features_mean[19]/1000000.0  ; /// 49 STDo**2
			sar_features_mean[103] =      sar_features_mean[7 ] / sar_features_mean[6 ]            ; /// 50 E_390  /E_75
			sar_features_mean[104] =      sar_features_mean[22] /(sar_features_mean[21]+0.0005)    ; /// 51 E_2000>/E_2000               
			sar_features_mean[105] =      sar_features_mean[20] / sar_features_mean[7 ]            ; /// 52 E_30   /E_390
			 
			sar_features_mean[106] =      (int)(Eaverageincut*1000000.)/1000000.                   ; /// 53 INCUT
			   if (Eaverageincut  > 99) Eaverageincut=99.0;   
			 
			sar_features_mean[107] =      sar_features_mean[1 ] * sar_features_mean[49]            ; /// 54 MEAN-INT * S-16
			sar_features_mean[108] =      sar_features_mean[1 ] * sar_features_mean[5 ]            ; /// 55 MEAN-INT * ENERGY-NN 
			sar_features_mean[109] =      sar_features_mean[1 ] * sar_features_mean[8 ]            ; /// 56 MEAN-INT * E_600    
			sar_features_mean[110] =      sar_features_mean[3 ] * sar_features_mean[24]	         ; /// 57 E_N      * LONGHIGG         

			sar_features_mean[111] =      sar_features_mean[3 ] * sar_features_mean[39]	         ; /// 58 E_N      * S-6     
			sar_features_mean[112] =      sar_features_mean[6 ] * sar_features_mean[14]			 ; /// 59 E_75     * GLCM_entrop	 
			sar_features_mean[113] =      sar_features_mean[4 ] * sar_features_mean[11]	         ; /// 60 E_r      * GLCM_mean   
			sar_features_mean[114] =      sar_features_mean[7 ] * sar_features_mean[36]	         ; /// 61 E_390    * S-3
			sar_features_mean[115] =      sar_features_mean[8 ] * sar_features_mean[8 ]	         ; /// 62 E_600** 
			sar_features_mean[116] =      sar_features_mean[9 ] * sar_features_mean[32]	         ; /// 63 STDn     * int*V
			sar_features_mean[117] =      sar_features_mean[9 ] * sar_features_mean[46]	         ; /// 64 STDn     * S-13
			sar_features_mean[118] =      sar_features_mean[10] * sar_features_mean[11]	         ; /// 65 Spec-Max * GLCM_mean
			sar_features_mean[119] =      sar_features_mean[11] * sar_features_mean[50]	         ; /// 66 GLCM_mean   * S-17       
			sar_features_mean[120] =      sar_features_mean[12] * sar_features_mean[21]	         ; /// 67 GLCM_var    * E_2000
			 
			sar_features_mean[121] =	  sar_features_mean[12] * sar_features_mean[39]            ; /// 68 GLCM_var    * S-6
			sar_features_mean[122] =	  sar_features_mean[14] * sar_features_mean[22]			   ; /// 69 GLCM_entrop * E_2000>
			sar_features_mean[123] =	  sar_features_mean[17] * sar_features_mean[22]            ; /// 70 GLCM_contr  * E_2000>
			sar_features_mean[124] =	  sar_features_mean[17] * sar_features_mean[26]            ; /// 71 GLCM_contr  * RELATION
			sar_features_mean[125] =	  sar_features_mean[17] * sar_features_mean[37]            ; /// 72 GLCM_contr  * S-4
			sar_features_mean[126] =	  sar_features_mean[20] * sar_features_mean[39]            ; /// 73 E_30        * S-6
			sar_features_mean[127] =	  sar_features_mean[35] * sar_features_mean[35]            ; /// 74 S-2**
			sar_features_mean[128] =	  sar_features_mean[36] * sar_features_mean[36]            ; /// 75 S-3** 
			sar_features_mean[129] =	  sar_features_mean[37] * sar_features_mean[37]            ; /// 75 S-4**     
			sar_features_mean[130] =	  sar_features_mean[38] * sar_features_mean[38]            ; /// 75 S-5**     
			 
//---------------------------------------------------------------------------------------------------			 
// 3.4. Metadata mean value over imagette S1 WV ( only for output needed) -mean value for used subscene coordinates   
//---------------------------------------------------------------------------------------------------	 
	 int count_plus=0; int count_minus=0; float LAT_plus=0; float LON_plus =0; float LAT_minus=0; float LON_minus=0; int count_land=0;
	 
	for (int ism=0; ism < 4; ++ism)
	{  // do-loop over metadata
		sar_metadata_mean[ism] = 0;      // 1D vector
		double value=0;int   zahler=0; 
   
  		for(unsigned int isl=0; isl<LINES_NUMBER;  ++isl)
		{     // do-loop over x coordinate in subscene 
		   
			value = value + sar_metadata_matrix[ism][isl] ;
			zahler= zahler +1 ;

			// cout<<"  XXX  MEADATA:"<< sar_metadata_matrix[ism][isl]<<" ism="<<ism<<"  isl="<<isl<<"   zahler"<<zahler<<"   value="<<value<< endl;
			
			if(sar_metadata_matrix[2][isl] > 0.0) 
			{
				count_plus = count_plus  + 1;    //   sar_metadata_matrix[2][isl] = LON  
																	LAT_plus  = LAT_plus    + sar_metadata_matrix[1][isl] ; 
																	LON_plus  = LON_plus    + sar_metadata_matrix[2][isl] ; 
																// cout<<" COORDINATES CHEK +:"<< count_plus<<" ism:"<<ism<<" i:"<<i<<" j:"<<j<< endl; 
			}
		    if(sar_metadata_matrix[2][isl] <= 0.0)
			{
				count_minus  = count_minus + 1; 
			                                                      LAT_minus = LAT_minus   + sar_metadata_matrix[1][isl] ; 
	                                                              LON_minus = LON_minus   + sar_metadata_matrix[2][isl] ;  
														         // cout<<" COORDINATES CHEK -:"<< count_minus<<" ism:"<<ism<<" i:"<<i<<" j:"<<j<< endl;		  
			}
																  
		    if(sar_metadata_matrix[3][isl]  ==  1)count_land = 1;  // ein Mal Land - raus!! 
			  
 		}

			sar_metadata_mean[ism] =  value/zahler;   // 1D vector   e.g (mean-IA, mean-LON, mean-LAT)  // ism=0 incidence angle;  ism=1 LAT; ism=2 LON; ism=3 LANDMASK
	
	}


			if(count_plus >  count_minus)
			{
				sar_metadata_mean[1] = LAT_plus/count_plus;
				sar_metadata_mean[2] = LON_plus/count_plus; 
			}
			
			if(count_plus <= count_minus)
			{
				sar_metadata_mean[1] = LAT_minus/count_minus;
				sar_metadata_mean[2] = LON_minus/count_minus; 
			}
				
			if(count_land == 1)           
			{
				sar_metadata_mean[3] = 1;                     
			}

			 /*
             cout<<"***   METADATA MEAN FINISHED: ***"<<endl; 
             cout<<"***  LAT = "<<sar_metadata_mean[1]<<endl; 
             cout<<"***  LON = "<<sar_metadata_mean[2]<<endl; 
             cout<<"***   IA = "<<sar_metadata_mean[0]<<endl; 
             cout<<"*** LAND = "<<sar_metadata_mean[3]<<endl; 
			 */
//===========================================================================================
//4. LIEAR REGRESSION > SEA STATE PARAMETERS  
//===========================================================================================

		float swh = 0; float tm0 = 0; float tm1 = 0; float tm2 = 0;  float sw1 = 0;  float sw2 = 0;  float sww = 0;  float tmw = 0; 

	for (int isf=0; isf < n_features;   isf++)       
	{ // do-loop over features  440

//----------------swh-------------------------------------------------------------------		   
		double feature_normalized = 0; double coeffitients= 0;
		if( sar_metadata_mean[0] < 30) { //wv1
			feature_normalized = (sar_features_mean[isf] - mean_swh_vector_1[isf])/std_swh_vector_1[isf]; 
			coeffitients = coefftients_swh_vector_1[isf] ;		
    	}

		if( sar_metadata_mean[0] > 30) {  //wv2
			feature_normalized = (sar_features_mean[isf] - mean_swh_vector_2[isf])/std_swh_vector_2[isf];		
			coeffitients = coefftients_swh_vector_2[isf] ;		
		}
		
		if(isf == 0) feature_normalized =1. ;

			swh = swh + feature_normalized * coeffitients ; 
 //if( sar_metadata_mean[0] < 30) cout<<"wv1 n="<<isf<<" feature="<<sar_features_mean[isf]<<"  mean="<<mean_swh_vector_1[isf]<<"  std="<<std_swh_vector_1[isf] <<"  normalized="<< feature_normalized<<" coeff="<<coeffitients<<" SWH="<< swh <<endl;
 //if( sar_metadata_mean[0] > 30) cout<<"wv2 n="<<isf<<" feature="<<sar_features_mean[isf]<<"  mean="<<mean_swh_vector_2[isf]<<"  std="<<std_swh_vector_2[isf] <<"  normalized="<< feature_normalized<<" coeff="<<coeffitients<<" SWH="<< swh <<endl;
//----------------Tm0-------------------------------------------------------------------			
		feature_normalized = 0; coeffitients= 0;
		if( sar_metadata_mean[0] < 30) { //wv1
			feature_normalized = (sar_features_mean[isf] - mean_tm0_vector_1[isf])/std_tm0_vector_1[isf]; 
			coeffitients = coefftients_tm0_vector_1[isf] ;		
		}

		if( sar_metadata_mean[0] > 30) {  //wv2
			feature_normalized = (sar_features_mean[isf] - mean_tm0_vector_2[isf])/std_tm0_vector_2[isf];		
			coeffitients = coefftients_tm0_vector_2[isf] ;		
		}
		
		if(isf == 0) feature_normalized =1. ;

			tm0 = tm0 + feature_normalized * coeffitients ; 
//----------------Tm1-------------------------------------------------------------------
		feature_normalized = 0;  coeffitients= 0;
		if( sar_metadata_mean[0] < 30) { //wv1
			feature_normalized = (sar_features_mean[isf] - mean_tm1_vector_1[isf])/std_tm1_vector_1[isf]; 
			coeffitients = coefftients_tm1_vector_1[isf] ;		
		}

		if( sar_metadata_mean[0] > 30) {  //wv2
			feature_normalized = (sar_features_mean[isf] - mean_tm1_vector_2[isf])/std_tm1_vector_2[isf];		
			coeffitients = coefftients_tm1_vector_2[isf] ;		
		}
		
		if(isf == 0) feature_normalized =1. ;
	
			tm1 = tm1 + feature_normalized * coeffitients ; 
//----------------Tm1-------------------------------------------------------------------
		feature_normalized = 0;  coeffitients= 0;
		if( sar_metadata_mean[0] < 30) { //wv1
			feature_normalized = (sar_features_mean[isf] - mean_tm2_vector_1[isf])/std_tm2_vector_1[isf]; 
			coeffitients = coefftients_tm2_vector_1[isf] ;		
    	}

		if( sar_metadata_mean[0] > 30) {  //wv2
			feature_normalized = (sar_features_mean[isf] - mean_tm2_vector_2[isf])/std_tm2_vector_2[isf];		
			coeffitients = coefftients_tm2_vector_2[isf] ;		
		}
		
		if(isf == 0) feature_normalized =1. ;
	
			tm2 = tm2 + feature_normalized * coeffitients ; 
//----------------sw1-------------------------------------------------------------------
		feature_normalized = 0;  coeffitients= 0;
		if( sar_metadata_mean[0] < 30) { //wv1
			feature_normalized = (sar_features_mean[isf] - mean_sw1_vector_1[isf])/std_sw1_vector_1[isf]; 
			coeffitients = coefftients_sw1_vector_1[isf] ;		
		}

		if( sar_metadata_mean[0] > 30) {  //wv2
			feature_normalized = (sar_features_mean[isf] - mean_sw1_vector_2[isf])/std_sw1_vector_2[isf];		
			coeffitients = coefftients_sw1_vector_2[isf] ;		
		}
		
		if(isf == 0) feature_normalized =1. ;
	
			sw1 = sw1 + feature_normalized * coeffitients ; 
//----------------sw2-------------------------------------------------------------------
		feature_normalized = 0; coeffitients= 0;
		if( sar_metadata_mean[0] < 30) { //wv1
			feature_normalized = (sar_features_mean[isf] - mean_sw2_vector_1[isf])/std_sw2_vector_1[isf]; 
			coeffitients = coefftients_sw2_vector_1[isf] ;		
		}

		if( sar_metadata_mean[0] > 30) {  //wv2
			feature_normalized = (sar_features_mean[isf] - mean_sw2_vector_2[isf])/std_sw2_vector_2[isf];		
			coeffitients = coefftients_sw2_vector_2[isf] ;		
		}
		
		if(isf == 0) feature_normalized =1. ;
	
			sw2 = sw2 + feature_normalized * coeffitients ; 
//----------------sww-------------------------------------------------------------------
		feature_normalized = 0;  coeffitients= 0;
		if( sar_metadata_mean[0] < 30) { //wv1
			feature_normalized = (sar_features_mean[isf] - mean_sww_vector_1[isf])/std_sww_vector_1[isf]; 
			coeffitients = coefftients_sww_vector_1[isf] ;		
		}

		if( sar_metadata_mean[0] > 30) {  //wv2
			feature_normalized = (sar_features_mean[isf] - mean_sww_vector_2[isf])/std_sww_vector_2[isf];		
			coeffitients = coefftients_sww_vector_2[isf] ;		
	    }
		
		if(isf == 0) feature_normalized =1. ;
	
			sww = sww + feature_normalized * coeffitients ; 
//----------------tmw-------------------------------------------------------------------
		feature_normalized = 0;  coeffitients= 0;
		if( sar_metadata_mean[0] < 30) { //wv1
			feature_normalized = (sar_features_mean[isf] - mean_tmw_vector_1[isf])/std_tmw_vector_1[isf]; 
			coeffitients = coefftients_tmw_vector_1[isf] ;		
		}

		if( sar_metadata_mean[0] > 30) {  //wv2
			feature_normalized = (sar_features_mean[isf] - mean_tmw_vector_2[isf])/std_tmw_vector_2[isf];		
			coeffitients = coefftients_tmw_vector_2[isf] ;		
		}
		
		if(isf == 0) feature_normalized =1. ;
	
			tmw = tmw + feature_normalized * coeffitients ; 
//----------------tmw---------------------------------------------------------------------
    }  // do-loop over linear regression
//----------------tmw---------------------------------------------------------------------	
		// cout<<"***  LINEAR REGRESION DONE    ***"<<endl; 
//=========================================================================================
//5. RESULTS MATRIX 
//========================================================================================= 
//-----------------------------------------------------------------------------------------	
//  5.1. Matrix results 
 
	results[0] = sar_metadata_mean[0]; //IA
	results[1] = sar_metadata_mean[1]; //lat
	results[2] = sar_metadata_mean[2]; //lon
	results[3] = sar_features_mean[9]; //  nv  variance
		
	results[4] = swh;  //1 
	results[5] = tm0;  //2 
	results[6] = tm1;  //3 
	results[7] = tm2;  //4 

	results[8] = sw1;  //5  
	results[9] = sw2;  //6 
	results[10]= sww;  //7

	results[11]= tmw;  //8 
/*	   
	cout<<"============================================================================="<<endl;
	cout<<"   RESULTS"<<" "<<n_parameters << "  parameters estimated"<<endl;
	cout<<"       IA="<< results[0]<<endl;
	cout<<"      LAT="<< results[1]<<"   LON="<<results[2]<< endl;
	cout<<" variance="<< results[3]<<endl;  
	cout<<"TOTAL   INTEGRATED PARAMETERS:"<<endl;
	cout<<"      SWH="<< results[4]<<endl;
	cout<<"      Tm0="<< results[5]<<endl;
	cout<<"      Tm1="<< results[6]<<endl;
	cout<<"      Tm2="<< results[7]<<endl;
	cout<<"PARTIAL INTEGRATED PARAMETERS:"<<endl;
	cout<<"      SW1="<< results[8]<<endl;
	cout<<"      SW2="<< results[9]<<endl;
	cout<<"      SWW="<< results[10]<<endl;
	cout<<"      Tmw="<< results[11]<<endl;
	cout<<"============================================================================="<<endl;
*/
      //  cout<<"***  RESULTS MATRIX STORED OK    ***" <<endl; 
//=========================================================================================
}  //end void  calulate_seastate_parametrs_s1wv 
//=========================================================================================
     
      
//=========================================================================================
//6. FILTERING AND WRITE RESULTS 
//========================================================================================= 
void CWave_s1_wv::output_seastate_parameters()	
{    
//------------------------------------------------------------------------------------------
//6.1.   Zuordnung/ assignment 	
//------------------------------------------------------------------------------------------        
	float IA =results[0];
	float LAT=results[1];
	float LON=results[2];
	float NV =results[3]*results[3];

	float SWH=results[4];	
	float Tm0=results[5];
	float Tm1=results[6];
	float Tm2=results[7];	

	float SW1=results[8];
	float SW2=results[9];
	float SWW=results[10];	
	
	float Tmw=results[11];	

	float min_nv_1 = 1.0  ; float min_nv_2 = 1.0 ; 
	float max_nv_1 = 2.0  ; float max_nv_2 = 1.4 ; 
	float min_swh  = 0.20 ; float max_swh  = 18.0; 
	float min_t    = 2.5  ; float max_t    = 18.0;
	
	int typ;  if( IA < 30 ) typ= 1;  if( IA > 30 ) typ= 2; 

	int pysical_filtering_swh=1;
	int set_maxmin=1;
	
//------------------------------------------------------------------------------------------
// 6.2. Flagging  
//------------------------------------------------------------------------------------------
	 int key= 0;   

//--------6.2.1. variance flagging ---------------------------------------------------------	 	 
	if( IA > 30.0 && NV < min_nv_2) key= -1; if( IA > 30.0 &&   NV > max_nv_2) key= 1; 
	if( IA < 30.0 && NV < min_nv_1) key= -1; if( IA < 30.0 &&   NV > max_nv_1) key= 1; 
//--------6.2.2. wind     flagging ---------------------------------------------------------	 				   
	if (sar_features_mean[2] < (minwind)) key= -5;  //wind < min wind
//--------6.2.3. NRCS, Rosenthal-Facktor, Land ---------------------------------------------	 	 
	if (sar_features_mean[1] < 0.01  )           key= -1;  // intencity == 0!
	if (sar_features_mean[4] > 5000.0)           key= -1;  // E_r
	if (sar_metadata_mean[3] == 1)               key= -10; // land 
	
//--------6.2.4. replace values according keys ---------------------------------------------	 
					if (key == -10){SWH = 0.; SW1 = 0.; SW2 = 0.; SWW = 0.; Tm0=0.; Tm1=0.; Tm2=0.; Tmw=0.;           }  //      outlier land
					if (SWH < -1.5){SWH = 0.; SW1 = 0.; SW2 = 0.; SWW = 0.; Tm0=0.; Tm1=0.; Tm2=0.; Tmw=0.; key= -1;  }  //      total outlier
	 if (key == -5){if (SWH < 0.2 ){SWH = 0.; SW1 = 0.; SW2 = 0.; SWW = 0.; Tm0=0.; Tm1=0.; Tm2=0.; Tmw=0.;           }
					if (SW1 < 0.0 ) SW1 = 0.;
					if (SW2 < 0.0 ) SW2 = 0.;
					if (SWW < 0.0 ) SWW = 0.;
					if (Tm0 < 1.0 ) Tm0 = 0.;
					if (Tm1 < 1.0 ) Tm1 = 1.;
					if (Tm2 < 1.0 ) Tm2 = 0.;
					if (Tmw < 1.0 ) Tmw = 0.;     		                                                              }  //wind  outlier
					if (key == -1 ){SWH = 0.; SW1 = 0.; SW2 = 0.; SWW = 0.; Tm0=0.; Tm1=0.; Tm2=0.; Tmw=0.;           }  //      outlier
					if (key == -10){SWH = 0.; SW1 = 0.; SW2 = 0.; SWW = 0.; Tm0=0.; Tm1=0.; Tm2=0.; Tmw=0.;           }  //      outlier land
	 
	if( key  >= 0 ) 
	{
	 
//--------6.2.5. filtering  physics--------------------------------------------------

		if(pysical_filtering_swh == 1) 
		{  

		// cout<<" FILTERING SWH ON"<<endl;

			if(SW1 > SWH ) SW1 =0.9 * SWH; //  preliminary filtering for swell and windse - it can not be highr than SWH! 
			if(SWW > SWH ) SWW =0.9 * SWH; //  preliminary filtering for swell and windse - it can not be highr than SWH! 
			if(SW2 > SW1 ) SW2 =0.9 * SW1; //  preliminary filtering for swell            - it can not be highr than SW1! 
		}

//--------6.2.6. min max values filtering --------------------------------------------------

		if(set_maxmin == 1) 
		{  // filter out 
			if(SWH < min_swh ){SWH = 0.1;   SW1 = 0.1; SW2 = 0.1; SWW = 0.1 ; key= -2;  }
			if(SWH > max_swh ){SWH = 0;     key= -3;  }	
	 
			if(SW1 < min_swh ){SW1 = 0;/* key= -1;*/}
			if(SW1 > max_swh ){SW1 = 0;/* key= -1;*/}
			if(SW2 < min_swh ){SW2 = 0;/* key= -1;*/}
			if(SW2 > max_swh ){SW2 = 0;/* key= -1;*/}
			if(SWW < min_swh ){SWW = 0;/* key= -1;*/}
			if(SWW > max_swh ){SWW = 0;/* key= -1;*/}

			if(Tm0 < min_t ){Tm0 = 0;/*  key= -1;*/}
			if(Tm0 > max_t ){Tm0 = 0;/*  key= -1;*/}
			if(Tm1 < min_t ){Tm1 = 0;/*  key= -1;*/}
			if(Tm1 > max_t ){Tm1 = 0;/*  key= -1;*/}
			if(Tm2 < min_t ){Tm2 = 0;/*  key= -1;*/}
			if(Tm2 > max_t ){Tm2 = 0;/*  key= -1;*/}
			if(Tmw < min_t ){Tmw = 0;/*  key= -1;*/}
			if(Tmw > max_t ){Tmw = 0;/*  key= -1;*/}
		}
		
		if(set_maxmin == 0) 
		{  // set to min-max values 
			if(SWH < min_swh ){SWH = min_swh; /* key= 1;*/}
			if(SWH > max_swh ){SWH = max_swh; /* key= 1;*/}

			if(SW1 < min_swh ){SW1 = 0.; /* key= 1;*/}
			if(SW1 > max_swh ){SW1 = 0.; /* key= 1;*/}
			if(SW2 < min_swh ){SW2 = 0.; /* key= 1;*/}
			if(SW2 > max_swh ){SW2 = 0.; /* key= 1;*/}
			if(SWW < min_swh ){SWW = 0.; /* key= 1;*/}
			if(SWW > max_swh ){SWW = 0.; /* key= 1;*/}

			if(Tm0 < min_t ){Tm0 = min_t; /* key= 1;*/}
			if(Tm0 > max_t ){Tm0 = max_t; /* key= 1;*/}
			if(Tm1 < min_t ){Tm1 = min_t; /* key= 1;*/}
			if(Tm1 > max_t ){Tm1 = max_t; /* key= 1;*/}
			if(Tm2 < min_t ){Tm2 = min_t; /* key= 1;*/}
			if(Tm2 > max_t ){Tm2 = max_t; /* key= 1;*/}
			if(Tmw < min_t ){Tmw = min_t; /* key= 1;*/}
			if(Tmw > max_t ){Tmw = max_t; /* key= 1;*/}
		}  
 
		if(SWW < 0.01) Tmw = 0.;
	 
	} //key >=0  active points only 

	cout<<"   ==================="<<endl;
	cout<<"   LAT="<<sar_metadata_mean[1]<<endl;
	cout<<"   LON="<<sar_metadata_mean[2]<<endl;
	cout<<"   --------------------"<<endl;
	cout<<"   SWH="<<SWH<<endl;
	cout<<"   Tm0="<<Tm0<<endl;
	cout<<"   Tm1="<<Tm1<<endl;
	cout<<"   Tm2="<<Tm2<<endl;
	cout<<"   SW1="<<SW1<<endl;
	cout<<"   SW2="<<SW2<<endl;
	cout<<"   SWW="<<SWW<<endl;
	cout<<"   Tmw="<<Tmw<<endl;
	cout<<"   -------------------"<<endl;
	cout<<"   key="<<key<<endl;
	cout<<"   ==================="<<endl;
	

	 //  cout<<"***  SEA STATE PARAMETERS EXTRACTED    ***" <<endl; 
	  

//=========================================================================================
//7. CREATE OUTPUT PARAMETERS
//========================================================================================= 
	unsigned int buffer = 600;
	scene = imagette_id.substr(0,64); // local file name 
	char SCENE[buffer];
	snprintf(SCENE,buffer, "%s",scene.c_str());  
 
	string DATE1 = scene.substr(15, 8); char DATE11[buffer]; snprintf(DATE11, buffer,"%s", DATE1.c_str()); 
	string TIME1 = scene.substr(24, 6); char TIME11[buffer]; snprintf(TIME11, buffer,"%s", TIME1.c_str());  

	string DATE2 = scene.substr(31, 8); char DATE22[buffer]; snprintf(DATE22, buffer,"%s",DATE2.c_str()); 
	string TIME2 = scene.substr(40, 6); char TIME22[buffer]; snprintf(TIME22, buffer,"%s",TIME2.c_str());
 
	string POLAR = scene.substr(12, 2); char POLARIZATION[buffer]; snprintf(POLARIZATION, buffer,"%s",POLAR.c_str());  
	//cout<<" POLARIZATION "<<POLARIZATION<<endl; 

	int descendig_ascending = 0; if( MeanHeadingAngle > 270 ) descendig_ascending = 1; 
	//cout<<" descendig_ascending "<<descendig_ascending<<endl; 
 
	int  r_orbit = 999; //relOrbit.c_str(); 

	/*
	cout<<"  scene ="<<scene<<endl;
	cout<<"  DATE 1="<<DATE1<<endl;
	cout<<"  DATE-TIME 11="<<DATE11<<"   "<< TIME11<< endl;
	cout<<"  DATE 2="<<DATE2<<endl;
	cout<<"  DATE-TIME 21="<<DATE22<<"   "<< TIME22<< endl;
	cout<<"  descendig_ascending="<<descendig_ascending<<" heading="<< MeanHeadingAngle<<endl; 
	cout<<"  RELATIVE ORBIT="<<r_orbit<<" POL="<<POLARIZATION<< endl;
	*/ 
//=========================================================================================
//8. OUTPUT INTO FILE
//========================================================================================= 

	char char3[buffer];char char4[buffer];

	snprintf(char3, buffer, "%s%s",                                                            product_id.c_str(), "_all-parameters_seastate.txt");
	snprintf(char4, buffer, "%s%s%s%s%s%s", out_dir.c_str(), "/", product_id.c_str(), "/cci/", product_id.c_str(), "_all-parameters_seastate.txt");

	string  tmpFilename2    (char3);           // local file name 
	string  depthfilenameS2 (char4);           // full  file name with path 

		// cout<<" OUT PATH:FILE= "<<depthfilenameS2 <<endl;

	FILE* outFile2;

	if(exists(depthfilenameS2))   
	{	            //   header for first vignette ;   
	}
	else 
	{
		outFile2 = fopen(depthfilenameS2.c_str(),"w");
		fprintf(outFile2,"%130s%3s\n","  key=0  OK | key= 1 nv>nv_max | key=-1 outlier, false | key=-2 SWH<SWH-min | key=-3 SWH>SWH-max | key=-5 wind < min-wind, false | key=-10  land points | POL:",POLARIZATION);
		fprintf(outFile2,"%130s\n","-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		fprintf(outFile2, "%12s%12s%12s%12s%12s%12s%12s%12s%12s%12s%12s%12s%12s%12s%12s%12s%70s%10s%14s\n", 
	      "LAT",   "LON",   "SWH"   ,"Tm0"   ,"Tm1",   "Tm2", "SW1","SW2","SWW","Tmw","key", "wv1/wv2","start date","start time","end date","end time", "scene ID","rel.orbit","des(0)/asc(1)");	
		fclose(outFile2);
	}


		outFile2 = fopen(depthfilenameS2.c_str(),"a");
		fprintf(outFile2, "%12.5f%12.5f%12.5f%12.5f%12.5f%12.5f%12.5f%12.5f%12.5f%12.5f%12i%12i%12s%12s%12s%12s%70s%10i%14i\n", 
	                       LAT,   LON,  SWH   ,Tm0, Tm1,  Tm2,  SW1,  SW2,  SWW,  Tmw, key,typ,DATE11,TIME11,DATE22,TIME22,SCENE,r_orbit, descendig_ascending);
		fclose(outFile2);
  
		//  cout<<" OUT FINISHED"<<endl; 
//========================================================================= 	
}	//end void output_seastate_parametrs_s1wv
//========================================================================= 



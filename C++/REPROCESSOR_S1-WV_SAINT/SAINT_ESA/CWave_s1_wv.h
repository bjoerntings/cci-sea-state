/* 
 * File:   CWave_s1_wv.h
 * Author: ting_bj
 *
 * Created on February 18, 2020, 1:25 PM
 */
#ifndef CWAVE_S1_WV_H
#define CWAVE_S1_WV_H

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <iterator>
#include <sys/stat.h>

#include "CImg.h"



using namespace std;


class CWave_s1_wv
{
public:
    CWave_s1_wv();
	       
 //AAA   cimg_library::CImg<double>* read_input_data(string input_path,string output_path,string scene_id, string imagette);
    void read_input_data(cimg_library::CImg<double>* sar_features_matrix, string imagette);
    void calulate_seastate_parameters(cimg_library::CImg<double>* sar_features_matrix);
    void output_seastate_parameters  ();
    
    void create_outdirs(string input_path,string output_path,string scene_id);
    void read_coeffitiens(); 


protected:
       
    //scene
    string ProductName;
    //scene metadata
    string polarisationLayer;
    string imageID;
    double headingAngle;
    string relOrbit;
    //ui
    int verbose_level = 10;
    int land_key;
    float minwind =1.0;
    int pysical_filtering_swh=0;
    int output_setting=1;
    int spoints_flag=0;
    float special_points_min_distance = 500;
	int n_sar_features=10; 
	int LINES_NUMBER =9; 
        
	string imagette_id,product_id, data_dir,out_dir,output_path,scene,feature_file;
	
		
	vector<vector <float> > sar_metadata_matrix ;
	
       
    vector<double> sar_features_mean; 
    vector<float>  sar_metadata_mean; 
	
    vector<float>  Incut; 
	double Eaverageincut=0;
	vector<float> HeadingAngle;
	float MeanHeadingAngle=0;
        
    vector<char> seastate_key;	
        
    vector<double> coefftients_swh_vector_1;  // 1D:  feature-number  swh 
    vector<double> coefftients_swh_vector_2;
    vector<double> mean_swh_vector_1; 
    vector<double> mean_swh_vector_2; 
    vector<double> std_swh_vector_1; 
    vector<double> std_swh_vector_2; 

    vector<double> coefftients_tm0_vector_1;  // 1D:  feature-number   tm0 mean period 
    vector<double> coefftients_tm0_vector_2;
    vector<double> mean_tm0_vector_1; 
    vector<double> mean_tm0_vector_2; 
    vector<double> std_tm0_vector_1; 
    vector<double> std_tm0_vector_2; 

    vector<double> coefftients_tm1_vector_1;  // 1D:  feature-number   tm1  period 
    vector<double> coefftients_tm1_vector_2;
    vector<double> mean_tm1_vector_1; 
    vector<double> mean_tm1_vector_2; 
    vector<double> std_tm1_vector_1; 
    vector<double> std_tm1_vector_2; 

    vector<double> coefftients_tm2_vector_1;  // 1D:  feature-number   tm2  period 
    vector<double> coefftients_tm2_vector_2;
    vector<double> mean_tm2_vector_1; 
    vector<double> mean_tm2_vector_2; 
    vector<double> std_tm2_vector_1; 
    vector<double> std_tm2_vector_2; 

    vector<double> coefftients_sw1_vector_1;  // 1D:  feature-number   tm2  period 
    vector<double> coefftients_sw1_vector_2;
    vector<double> mean_sw1_vector_1; 
    vector<double> mean_sw1_vector_2; 
    vector<double> std_sw1_vector_1; 
    vector<double> std_sw1_vector_2; 

    vector<double> coefftients_sw2_vector_1;  // 1D:  feature-number   tm2  period 
    vector<double> coefftients_sw2_vector_2;
    vector<double> mean_sw2_vector_1; 
    vector<double> mean_sw2_vector_2; 
    vector<double> std_sw2_vector_1; 
    vector<double> std_sw2_vector_2; 	

    vector<double> coefftients_sww_vector_1;  // 1D:  feature-number   tm2  period 
    vector<double> coefftients_sww_vector_2;
    vector<double> mean_sww_vector_1; 
    vector<double> mean_sww_vector_2; 
    vector<double> std_sww_vector_1; 
    vector<double> std_sww_vector_2;

    vector<double> coefftients_tmw_vector_1;  // 1D:  feature-number   tm2  period 
    vector<double> coefftients_tmw_vector_2;
    vector<double> mean_tmw_vector_1; 
    vector<double> mean_tmw_vector_2; 
    vector<double> std_tmw_vector_1; 
    vector<double> std_tmw_vector_2;  
	
 
    vector<float> results;
    
    int set_min_max=0;
    vector<float> min_nv;
    vector<float> max_nv;
    vector<float> swh_min_max;
    vector<float> t_min_max;
    
    //sp
    int numer_of_spoints=0;
    vector<string> spoints_name;
    vector<float> spoints_geo_lon_in;
    vector<float> spoints_geo_lat_in;
    vector<float> spoints_geo_lon_out;
    vector<float> spoints_geo_lat_out;
    
private:
    inline bool exists(const std::string &filename)
    {
        struct stat buf;
        return (stat(filename.c_str(), &buf) == 0);
    }

    inline std::vector<std::string> splitString(std::string input)
    {
        std::vector<std::string> split;
        std::istringstream iss(input);
        std::copy(std::istream_iterator<std::string>(iss), 
                std::istream_iterator<std::string>(), 
                std::back_insert_iterator<std::vector<std::string> >(split));
        return split;
    }

    /**
     * Struct to hold the filtered results for netCDF & GeoJSON output
     */
	struct s_results {
		float lat;
		float lon;
		float swh;
	};

    /**
     * Save the result as netCDF file. Hides base class function.
     * @param ncFileName netCDF output file name
     * @param results struct holding the results
     */
    void saveResultsToNetCDF(string ncFileName, const s_results &results);

    /**
     * Save the results as GeoJSON file. Hides base class function.
     * @param GeoJSONFile GeoJSON output file name
     * @param results struct holding the results
     * @param sceneID product name
     * @param timeStamp UTC timestamp of the acquisition, should follow ISO 8601
     */
    void saveResultsToGeoJSON(string GeoJSONFile, const s_results &results, string sceneID, string timeStamp);

};

#endif /* CWAVE_S1_WV_H */


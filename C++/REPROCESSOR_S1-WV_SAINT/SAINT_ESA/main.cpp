/*
  DLR 2021, Sea State Reprocessor (SSR)

  This programs reprocesses 8 sea state parameters using pre-processed SAR-features by CWAVE_EX linear regression method.
  The SAR features are stored in files for each S1 WV imagette "imagette_ID_results.txt". 
  Each overfligth/product with Scene_ID includes 30-190 individual imagettes. 
  
  The program complement the data with sea state products, therefore the paths INPUT-DIR and OUTPUT-DIR must be equal: INPUT-DIR=OUTPUT-DIR.
  The example "input-data" include only feture-files(ID_results.txt) but not the sea state resuluts, the results are stored in new created 
  dir "cci" for each prodict-ID:  ../../../../S1-WV_DATA/Scene-ID/cci/Scene-ID._all-parameters_seastate.txt
  
  Run command:  ./saint_esa   INPUT-DIR                  OUTPUT-DIR              ´ Scene-ID
  e.g.:         ./saint_esa   ../../../../../S1-WV_DATA ../../../../../S1-WV_DATA  S1A_WV_SLC__1SSV_20171202T023627_20171202T030040_019517_021200_5EC2
      
 */

/* 
 * File:   main.cpp
 * Author: saint
 * Created on September 21, 2021, 12:14 PM
 */

#include <cstdlib>

#include <iostream>
#include <fstream>
#include <string>

#include "CWave_s1_wv.h"
#define INPUTBUFFER 1000

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
	cout<<"==============================================================="<<endl;
	cout<<" SAINT SENTINEL-1 WV                                           "<<endl;
	cout<<" PROCESSING SEA STATE FROM FEATURE FILES                       "<<endl;
	cout<<" DLR 2021   V1 LINEAR REGRESSION                               "<<endl;
	cout<<"==============================================================="<<endl;

	
	if(argc == 4)
	{
		// printf(" input-path: %s\n",argv[1]);
		// printf("output-path: %s\n",argv[2]);
		// printf("   SCENE-ID: %s\n",argv[3]);
		 
//=======================================================================
// 1. CREATE  PATHS  		
//=======================================================================		
//		char INPUT_PATH[200];  sprintf(INPUT_PATH,  "%s", argv[1]); 
//		char OUTPUT_PATH[200]; sprintf(OUTPUT_PATH, "%s", argv[2]); 
//		char SCENE_ID[80];     sprintf(SCENE_ID,    "%s", argv[3]); 
//		char IMAGETTE_ID[100];
		
		string S_INPUT_PATH(argv[1]);
		string S_OUTPUT_PATH(argv[2]);
		string S_SCENE_ID(argv[3]);


		cout<<"====================================================="<<endl;
		cout<<"  INPUT PATH = "<<S_INPUT_PATH<<endl;
		cout<<" OUTPUT PATH = "<<S_OUTPUT_PATH<<endl;
		cout<<"  SCENE      = "<<S_SCENE_ID<<endl;
		cout<<"====================================================="<<endl;
//=======================================================================
// 2. READ COEFFITIENTS 	
//=======================================================================              
		CWave_s1_wv processor;
		processor.read_coeffitiens();

//=======================================================================           
// 2. CREATE OUT DIR    
//======================================================================= 
	processor.create_outdirs(S_INPUT_PATH,S_OUTPUT_PATH,S_SCENE_ID);
//=======================================================================
// 3. CREATE IMAGETTES LIST 	
//=======================================================================
		unsigned int buffersize = 300;
		char command[buffersize];
			//sprintf(command, "%s%s%s%s%s","ls ", argv[1], "/",argv[3],"/s*.txt  > imagettes_id_list.txt ");
		snprintf(command, buffersize, "%s%s%s%s%s","ls ", argv[1], "/",argv[3]," > imagettes_id_list.txt ");
			//cout<<"command: " <<command<<endl; 
		system(command);
	
//=======================================================================
// 4. READ IMAGETTES LIST 	
//=======================================================================	
	
		char LINE[INPUTBUFFER];
		char CCI[] = "cci";
		int lines=0;
	
		string id_list_file= "imagettes_id_list.txt";
	
		ifstream imagette_list_file; 
		imagette_list_file.open (id_list_file.c_str());
			 
		int file_lines_n=9; int n_p_features=54;		 
		cimg_library::CImg<double>* sar_features_matrix;
		sar_features_matrix = new cimg_library::CImg<double>(n_p_features, file_lines_n);

		while (imagette_list_file.getline(LINE, INPUTBUFFER))
		{
		//  cout<<"LIST LINE="<<LINE<<"  CCI="<<CCI<< endl;
		if (LINE[1] != CCI[1])
		{

			lines = lines + 1;
			// sprintf(IMAGETTE_ID,"%s",LINE);

			string S_IMAGETTE_ID(LINE);

			// cout<<"IMAGETTE_ID="<<IMAGETTE_ID<<endl;

			string IMAGE_N = string(LINE);
			string image_n = IMAGE_N.substr(61, 3);
			string image_t = IMAGE_N.substr(4, 3);

//=======================================================================
// 4. PROCESS INDIVIDUAL IMAGETTES
//=======================================================================

//AAA   cimg_library::CImg<double>* sar_features_matrix = processor.read_input_data(INPUT_PATH,OUTPUT_PATH,SCENE_ID,IMAGETTE_ID);

			processor.read_input_data(sar_features_matrix, S_IMAGETTE_ID);
			// cout<<"&&&&& read_input_data               OK  &&&&"<<endl;
			processor.calulate_seastate_parameters(sar_features_matrix);
			// cout<<"&&&&& calulate_seastate_parameters  OK  &&&&"<<endl;
			processor.output_seastate_parameters();
			// cout<<"&&&&& output_seastate_parameters   OK  &&&&"<<endl;

			cout << "IMAGETTE PROCESSED:  N =" << image_n << "  type=" << image_t << endl;
			cout << "         " << endl;
		}
		}
		imagette_list_file.close();
	 
		delete sar_features_matrix ;
	
		cout<<"================================"<<endl;
		cout<<"+++ ALL IMAGETTES PROCESSED  +++"<<endl;
		cout<<"ID: "<<S_SCENE_ID                <<endl;
		cout<<"================================"<<endl;
//-----------------------------------------------------------------------
//   END PROCESS  IMAGETTES  		
//-----------------------------------------------------------------------
		
		
	}
	 else{
		 cout << "run with: ./executable  input-path                         output-path               SCENE-ID" <<endl;
		 cout << "E.g: ./saint_esa   ../../../../../S1-WV_INPUT ../../../../S1-WV_OUTPUT  S1A_WV_SLC__1SSV_20171202T023627_20171202T030040_019517_021200_5EC2 " << endl;
	 }
    return 0;
}


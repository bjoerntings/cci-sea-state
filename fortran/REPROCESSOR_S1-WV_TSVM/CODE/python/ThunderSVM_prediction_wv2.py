# -*- coding: utf-8 -*-
import sys
import glob
import os
import math  
from pathlib import Path
from sklearn.metrics import mean_squared_error
from datetime import date
import time

from thundersvm import *
from sklearn.datasets import *


def main(argv):
    #Documentation can be found at:
    #https://github.com/Xtra-Computing/thundersvm/tree/master/python
    
    #print ("Thunder-SVM prediction using trained model") 

# data path and file neames


    model_Folder    = "../tsvm_models/"                                                                         # model folder/path
    model_File      = "tsvm_wv2_normalized_gamm-0075_cost-25_tol-01"                                            # model name       
     
    input_Folder    = "../tmp/tvsm_input/"                                                                      # validation input data folder/path 
    input_File      = "tsvm_input_wv2_swh_tmp.txt"                                                              # tsvm input file name   

    
    results_Folder  = "../tmp/tsvm_prediction/"                                                                 # prediction/reuslts/output folder/path 
    results_File    = os.path.join(results_Folder +'tsvm_predicted_wv2.txt')                                    # prediction/reuslts/output file 
    
    protocol_Folder = results_Folder                                                                            # protocol Folder               !!!!!!!!!
    protocol_File   = "phyton_tsvm_wv2_prot.txt"                                                                # protocol File name 
    

    tsvm_model      = os.path.join(model_Folder,    model_File+'.model')
    tsvm_input      = os.path.join(input_Folder,   input_File          )
    prot_file       = os.path.join(protocol_Folder,protocol_File       )
  
    aktuellesDatum = date.today()
   #print (aktuellesDatum) 
 
    aktuellesTime1= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
   #print (aktuellesTime) 

  #  print ("data path       OK") 
  #  print("============================="           )
  #  print("       TSVM-MODEL: "+tsvm_model           )
  #  print("  VALIDATION DATA: "+tsvm_input           )
  #  print("PREDICTED RESULTS: "+results_File         )
  #  print("============================="           )
    
# read data  
    x_test,y_test=load_svmlight_file(tsvm_input)                                 # data input 
    aktuellesTime0= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    print ("read input data  OK "+aktuellesTime0)   

# read model  
    svr = NuSVR(kernel = 'rbf', gamma = 0.005, nu = 0.5, C = 25.0, tol = 0.01)
    svr.load_from_file(tsvm_model)
    aktuellesTime1= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    print ("tsvm read model  OK "+aktuellesTime1 )
    
#prediction
    y_predict=svr.predict(x_test)
    aktuellesTime2= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    print ("tsvm prediction  OK "+aktuellesTime2)
  
#write results    
    y_predict.tofile(results_File, sep = '\n')
    aktuellesTime3= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    print ("write results    OK "+aktuellesTime3)
    
  # score=svr.score(x_test,y_test)
  # print ("test score is ", score)
  ##  print("MSE and RMSE:")
  ##  print("MSE : ", mean_squared_error(y_test,y_predict))
  ##  print("RMSE: ", math.sqrt(mean_squared_error(y_test,y_predict)))
  ##  aktuellesTime4= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    
    
#write proticol file (log)   
  ##  rmse= str(math.sqrt(mean_squared_error(y_test,y_predict)))
  ##  aktuellesTime5= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    
  # print("protocol  file: ",prot_file)
    f = open(prot_file, 'w')
    f.write("============================="  +"\n")
    f.write("Thunder-SVM validation"+"\n")
    f.write("============================="  +"\n")
  # f.write("    TRAINING DATA:")
    f.write("       TSVM-MODEL: "+tsvm_model  +"\n")
    f.write("  VALIDATION DATA: "+tsvm_input  +"\n")
    f.write("PREDICTED RESULTS: "+results_File+"\n")   
    f.write("============================="  +"\n")
    f.write(" START TIME: "+aktuellesTime0    +"\n")
    f.write("  READ DATA: "+aktuellesTime1    +"\n") 
    f.write(" PREDICTION: "+aktuellesTime2    +"\n") 
    f.write("    RESULTS: "+aktuellesTime3    +"\n")  
  #  f.write("       RMSE: "+aktuellesTime4    +"\n")     
  #  f.write("============================="  +"\n")
  #  f.write("RMSE="+rmse                     +"\n")

    f.close()

  
    
if __name__ == '__main__':
    main(sys.argv[1:])
#=======================================================================================================================   
#models:      /bffs01/group/intern/users/ples_ad/SRC/CCI/REPROCESSING/tsvm/TSVM_REPROCESSOR_1M/tsvm_models/
#data:        /bffs01/group/intern/users/ples_ad/SRC/CCI/REPROCESSING/tsvm/TSVM_REPROCESSOR_1M/tsvm_input/         tsvm_input_wv2__swh-d1____wind_d1__tmp.txt
#prediction:  /bffs01/group/intern/users/ples_ad/SRC/CCI/REPROCESSING/tsvm/TSVM_REPROCESSOR_1M/tsvm_prediction/    tsvm_predicted_wv2.txt  
#=======================================================================================================================   
#  ~/miniconda3/bin/python       ../phyton/ThunderSVM_prediction_wv2.py


!===============================================  
PROGRAM REPROCESSOR
!===============================================
! EXAMPLE RUN:
! ./TSVM_REPROCESSOR.out S1B_WV_SLC__1SSV_20200426T014658_20200426T021406_021308_02872A_2914
!===============================================
character year*4, month*2
character (len=220) :: com_01
character (len=300) :: com_02
character (len=400) :: com_phytopn_wv1
character (len=400) :: com_phytopn_wv2

character (len=300) :: python_lib_path,com_processor_main

CHARACTER (len=67)  :: scene_id,get_parameter
integer i,header
!===============================================
write(*,*)'==============================================='
write(*,*)'REPROCESSING SWH USING Tunder-SVM MODELS (python)'
write(*,*)'==============================================='
    i=0
	DO
	call get_command_argument(i,get_parameter)
      IF (LEN_TRIM(get_parameter) == 0) EXIT
	    if(i==1) scene_id = get_parameter
        !write(*,*)'scene id = ',scene_id,'  ii= ',ii
	   i = i+1
    END DO
	    if(i.lt.2)then
		  write(*,*)"SCENE ID IS NOT DEFINED!"
		  write(*,*)"run with: ./executable  SCENE-ID "
		  write(*,*)"E.g.: ./TSVM_REPROCESSOR.out S1B_WV_SLC__1SSV_20200426T014658_20200426T021406_021308_02872A_2914"
		  write(*,*)"STOP END"
		  goto 999
		end if
	    write(*,*)'scene id=',scene_id

write(*,*)'=========================================='
write(*,*)'1. DATA READ and RESTORING TSVM-FORMAT'
write(*,*)'=========================================='
write(com_processor_main,*)'./tsvm_processor_1M_main.out ',scene_id
call system(com_processor_main)


write(*,*)'=========================================='
write(*,*)'2. USING TSVM MODEL WITH python WV-1'
write(*,*)'=========================================='

open(1,file="../python/python_lib_path.txt")
read(1,'(a300)')python_lib_path
close(1)

write(com_phytopn_wv1,*)python_lib_path,'  ../python/ThunderSVM_prediction_wv1.py'
write(*,*)'PYTHON: ',com_phytopn_wv1
call system(com_phytopn_wv1)
 
write(*,*)'=========================================='
write(*,*)'3. USING TSVM MODEL WITH python WV-2'
write(*,*)'=========================================='
write(com_phytopn_wv2,*)python_lib_path,'  ../python/ThunderSVM_prediction_wv2.py'
write(*,*)'PYTHON: ',com_phytopn_wv2
call system(com_phytopn_wv2)
 
write(*,*)'=========================================='
write(*,*)'4. REPLACE SWH IN RESULTS-FILE'
write(*,*)'=========================================='

call system('./tsvm_cut-results_1M_wv1.out')
call system('./tsvm_cut-results_1M_wv2.out')

write(*,*)'=========================================='
write(*,*)'5. CHECK LANDMASK FOR CONTINENTS'
write(*,*)'=========================================='

call system('./tsvm_results_map-check.out')


write(*,*)'=========================================='
write(*,*)'6. STORE RESULTS '
write(*,*)'=========================================='


open(99, file='../settings_and_headers/options.txt')
read(99,*)header
close(99)


if(header.ne.1)write(com_02,'(a110,a13,a67,a28)') 'cat ../tmp/tmp_results/results_wv1_land-cheked.txt       &
                                                       ../tmp/tmp_results/results_wv2_land-cheked.txt > '   ,&
												     '../../OUTPUT/',scene_id,'_all-parameters_seastate.txt'
														
if(header.eq.1)write(com_02,'(a40,a110,a13,a67,a28)') 'cat ../settings_and_headers/header.txt '             ,& 
                                                   '../tmp/tmp_results/results_wv1_land-cheked.txt            &
                                                    ../tmp/tmp_results/results_wv2_land-cheked.txt > '       ,&
												   '../../OUTPUT/',scene_id,'_all-parameters_seastate.txt'								
							
call system(com_02)	

			   
	

write(*,*)''
write(*,*)'     RE-PROCESSING FINISHED '
write(*,*)'=========================================='

999 continue
STOP
END
!============================================================
                     PROGRAM PROCESSOR 
!============================================================
!   options:   ../settings_and_headers/options.txt
!============================================================
character dum1*1,dum10*10,dum100*100
integer IOO,IO,IOOO,weka_header
logical :: exist
integer, save:: XXX(300)

CHARACTER (len=67),save   :: scene_id,get_parameter
CHARACTER (len=64),save   :: imagette_id

character (len=500),save  :: DIR
character (len=70),save   :: DIR_LIST
character (len=120),save  :: IDN,IIDN
character (len=250),save  :: RESUT_FILE,FEATURE_FILE
character (len=250),save  :: coeff_file1,coeff_file2
character(len=21),save    :: SWH_str


real,save      :: LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw
integer,save   :: key,wv1_wv2,s_date,s_time,e_date,e_time,orbit,DIR_LENGTH
integer,save   :: des_asc,i_i,i_p,i_i1,i_i2,i_read,n_ID,n_IDnew
character,save ::  n_ID_char*3
real,save      :: FMIN2(53),FMAX2(53),FMIN1(53), FMAX1(53)

logical :: file_exists
!============================================================
real,save :: LATT,LONN,PL,SWH_o,SWH_c,DIRR,HEADING,E30_75,E75_390,E390_600,GODA,LONG_HIG,Tmean
real,save :: NRCS,dB,WIND,INC_ANGLE,E_M,E_N,E_R,E_NN,FG_DEPTH,mean_wind,bbb,SWHn
real,save :: meanincut,meanout,cut_out,STD_N,SPEC_MAX,STD_O,E1_30,E600_2000,E2000
real,save :: GL_MEAN, GL_VAR,GL_CORR,GL_ENTROPY,GL_HOMOGEN,GL_ENERGY,GL_CONTRAST,GL_DISSIMIL
real,save :: CONVOLUT,MEAN_DIR,relation, Sy_Sx,SKEW,KURT,cutoff, p50_10000,INT_V,INT_LOG
real,save :: S_1,S_2,S_3,S_4,S_5,S_6,S_7,S_8,S_9,S_10,S_11,S_12,S_13,S_14,S_15,S_16,S_17,S_18,S_19,S_20
integer,save :: i_w1,i_w2,j

double precision :: P(53,9),PM(53)
double precision,save :: MEAN1(53),MEAN2(53), STD1(53),STD2(53)
character :: PMC(52)*20

real,save :: swh_1_min,swh_1_max, swh_2_min,swh_2_max,wind_1_min,wind_1_max, wind_2_min,wind_2_max    

integer,save :: LINE, POINT, ROW_Y,COL_X,iS1AB
character,save :: FLAG*1
character(len=9),save   :: string01
character(len=20),save  :: string02
character(len=20),save  :: string03
character(len=20),save  :: string04
character(len=16),save  :: string05
character(len=220),save :: COMMAND_LIST
character(len=4),save   :: YEAR
character(len=2),save   :: MONTH

character(len=7)  :: str_iS1AB
character(len=12) :: str_LAT,str_IA
real,save :: sin_lat, INANGLE(9),IN_ANGLE
real,save :: FMIN(150),FMAX(150)
character,save :: S1AB*1
!============================================================
!  open files for tsvm-format and for results 
!============================================================
 	open(765, file='../settings_and_headers/options.txt')
 		read(765,*)dum1
 		read(765,'(a100)')coeff_file1   ! wv1
 		read(765,'(a100)')coeff_file2   ! wv2
 		read(765,*)dum1
 		read(765,*)dum1
 		read(765,*) swh_1_min,swh_1_max
 		read(765,*) swh_2_min,swh_2_max
 		read(765,*)dum1
 		read(765,*)dum1
 		read(765,*) wind_1_min,wind_1_max
 		read(765,*) wind_2_min,wind_2_max
 	close(765)

!---- read mean and std 
	open(81, file=coeff_file1)
	open(82, file=coeff_file2)
	do i=1,11
		read(81,*)dum1
		read(82,*)dum1
	end do
	
		read(81,*) bbb, (MEAN1(i),i=1,53)
		read(82,*) bbb, (MEAN2(i),i=1,53)
		read(81,*) bbb, ( STD1(i),i=1,53)
		read(82,*) bbb, ( STD2(i),i=1,53)
	  
	close(81)
	close(82)
	
!---read min-max-------------------------- 
	open(717,file='../settings_and_headers/features_min_max_wv1.txt')
		read(717,*)dum1
		read(717,*)(FMIN1(ix),ix=1,53)
		read(717,*)(FMAX1(ix),ix=1,53)
	close(717)
	
	open(718,file='../settings_and_headers/features_min_max_wv2.txt')
		read(718,*)dum1
		read(718,*)(FMIN2(ix),ix=1,53)
		read(718,*)(FMAX2(ix),ix=1,53)
	close(718) 

!---open tmp file for writing stvm inputs ------------------------ 

	open(212, file='../tmp/tvsm_input/tsvm_input_wv1_swh_tmp.txt')
	open(222, file='../tmp/tvsm_input/tsvm_input_wv2_swh_tmp.txt')
  
!---open txt results file ---------------- 

	open(312, file='../tmp/tmp_data/results-and-id_wv1_swh_tmp.txt')
	open(322, file='../tmp/tmp_data/results-and-id_wv2_swh_tmp.txt')
	 
!============================================================
!  S1-data
!============================================================
	open(456, file='../settings_and_headers/data_path.txt')
		read(456,'(a500)')DIR
	close(456)
	
DIR_LENGTH=LEN_TRIM(DIR)
!write(*,*)'length dir=',DIR_LENGTH

	ii=0
	DO
	call get_command_argument(ii,get_parameter)
		IF (LEN_TRIM(get_parameter) == 0) EXIT
		if(ii==1) scene_id = get_parameter
		!write(*,*)'scene id = ',scene_id,'  ii= ',ii
		ii = ii+1
	END DO
	
	!write(*,*)'Scene id: ',scene_id 

!============================================================
! 1. open dir 
!============================================================
	i_i=0 
	i_p=0 
	i_i1=0
	i_i2=0

	i_w1=0
	i_w2=0

	i=1

	i_p=i_p+1
!============================================================
! 2. in dir 
!============================================================
!  P1        P2
! s1a-wv1-slc-vv-20181103t100757-20181103t100800-024422-02ad0f-001_results.txt
! S1A_WV_SLC__1SSV_20181101T025243_20181101T032711_024388_02ABD6_3954_all-parameters_seastate.txt
! S1A_WV_SLC__1SSV_20181103T100757_20181103T101308_024422_02AD0F_7FE8   IDN
! 1234567890123456789012345678901234567890123456789012345678901234567
!         10        20        30        40        50        60     67
! 
 ! write(*,*)i_p, DIR,IDN 
	IDN=scene_id  
	IIDN=IDN  ! scene_id
  
  
	read(IDN(3:4),*)S1AB
	!write(*,*)'S1 = S1',S1AB
	if(S1AB=='A')iS1AB=0
	if(S1AB=='B')iS1AB=1
	 
	read(IDN(18:21),*)YEAR
	read(IDN(22:23),*)MONTH
  !write(*,*)'YEAR= ',YEAR,'    MONTH= ',MONTH
  
	if (DIR_LENGTH.lt.10 )                          write(string02,'(a2,i1,a16)')'(a',DIR_LENGTH,',a67,a5,a67,a28)'
	if((DIR_LENGTH.ge.10 ).and.(DIR_LENGTH.lt.100)) write(string02,'(a2,i2,a16)')'(a',DIR_LENGTH,',a67,a5,a67,a28)'
	if (DIR_LENGTH.ge.100)                          write(string02,'(a2,i3,a16)')'(a',DIR_LENGTH,',a67,a5,a67,a28)'
  
	write(RESUT_FILE,string02)DIR,IDN,'/cci/',IDN,'_all-parameters_seastate.txt'    ! to make list of imagettes 
  

	INQUIRE(FILE=RESUT_FILE, EXIST=file_exists)
	IF(file_exists)THEN
	   	   
		open(11, file=RESUT_FILE)  !  _all-parameters_seastate.txt
		read(11,'(a100)')dum100
		read(11,'(a100)')dum100
		read(11,'(a100)')dum100
 
 
 XXX(:)=0
 do 100 ! read imagete ID (ID)
 read(11,*,IOSTAT=IO ) LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2,  s_date,s_time,e_date,e_time,imagette_id,orbit,des_asc    ! read _all-parameters_seastate.txt with ID
		if (IO /= 0)exit 
			read(imagette_id(62:64),*)n_ID_char
			read(n_ID_char,'(i3)')n_IDnew
		
		if(XXX(n_IDnew) == 1) then
			write(*,*)'DOUBLE PROCESSING:',imagette_id,n_IDnew 
			goto 100
	   endif

		XXX(n_IDnew)= 1
	   
		i_i = i_i +1 
		if(wv1_wv2==1)i_i1 = i_i1 +1 
		if(wv1_wv2==2)i_i2 = i_i2 +1 
		   

		write(string03,'(a2,i2,a16)')'(a',DIR_LENGTH, ',a67,a1,a64,a12)'
		write(FEATURE_FILE,string03)DIR,IDN,'/',imagette_id,'_results.txt'    

	   
		open(111, file=FEATURE_FILE) ! _results.txt 
			do i=1,22
				read(111,*)dum1
			end do
		
		i_read=0
		P =0
do 777
		!do i=1,9
read (111,888,IOSTAT=IOOO) LATT,LONN,PL,SWH_o,SWH_c,DIRR,LINE,POINT,ROW_Y,COL_X,NRCS,dB,WIND,IN_ANGLE,E_M,E_N,E_R,E_NN,FG_DEPTH, & !  read features from individual results-files
						FLAG, &
						HEADING, E30_75,E75_390,E390_600,meanincut,meanout,cut_out,STD_N,SPEC_MAX, &
						GL_MEAN, GL_VAR,GL_CORR,GL_ENTROPY,GL_HOMOGEN,GL_ENERGY,GL_CONTRAST,GL_DISSIMIL, &
						STD_O,E1_30,E600_2000,E2000,GODA,LONG_HIG,Tmean,CONVOLUT,MEAN_DIR,relation,&
 						Sy_Sx,SKEW,KURT,cutoff, p50_10000,INT_V,INT_LOG, &
						S_1,S_2,S_3,S_4,S_5,S_6,S_7,S_8,S_9,S_10,S_11,S_12,S_13,S_14,S_15,S_16,S_17,S_18,S_19,S_20
		if (IOOO /= 0)exit 
		
		i_read=i_read+1 

888        FORMAT(2F15.6,F10.1,2F10.2,F10.1,4i10,F10.2,F10.3,2F10.1,2F10.3,F10.1,F10.5,F10.1, A10, &
               F20.1,7F10.6,F10.2,2F10.4,6F10.5,F10.1,7F10.6,F10.2,2F10.6,2F10.2,F10.1,3F10.1,20F10.2) 						
						
			!write(*,*)LATT,LONN, SWH_c,	S_1, S_20
			
			INANGLE(i_read)= IN_ANGLE
			
			P( 1,i_read)=NRCS
			P( 2,i_read)=WIND
			P( 3,i_read)=E_N
			P( 4,i_read)=E_R
			P( 5,i_read)=E_NN
			P( 6,i_read)=E30_75
			P( 7,i_read)=E75_390
			P( 8,i_read)=E390_600
			P( 9,i_read)=STD_N
			P(10,i_read)=SPEC_MAX

			P(11,i_read)=GL_MEAN 
			P(12,i_read)=GL_VAR
			P(13,i_read)=GL_CORR
			P(14,i_read)=GL_ENTROPY
			P(15,i_read)=GL_HOMOGEN
			P(16,i_read)=GL_ENERGY
			P(17,i_read)=GL_CONTRAST
			P(18,i_read)=GL_DISSIMIL
			P(19,i_read)=STD_O
			P(20,i_read)=E1_30
			
			P(21,i_read)=E600_2000
			P(22,i_read)=E2000
			P(23,i_read)=GODA
			P(24,i_read)=LONG_HIG
			P(25,i_read)=CONVOLUT
			P(26,i_read)=relation
			P(27,i_read)=Sy_Sx
			P(28,i_read)=SKEW
			P(29,i_read)=KURT
			P(30,i_read)=cutoff
			
			P(31,i_read)=p50_10000
			P(32,i_read)=INT_V
			P(33,i_read)=INT_LOG
			P(34,i_read)=S_1
			P(35,i_read)=S_2
			P(36,i_read)=S_3
			P(37,i_read)=S_4
			P(38,i_read)=S_5
			P(39,i_read)=S_6
			P(40,i_read)=S_7
			
			P(41,i_read)=S_8
			P(42,i_read)=S_9
			P(43,i_read)=S_10
			P(44,i_read)=S_11
			P(45,i_read)=S_12
			P(46,i_read)=S_13
			P(47,i_read)=S_14
			P(48,i_read)=S_15
			P(49,i_read)=S_16
			P(50,i_read)=S_17
			
			P(51,i_read)=S_18
			P(52,i_read)=S_19
			P(53,i_read)=S_20

777     continue 		
		close(111)
		
			if(i_read==0) then
			  if(key.lt.0) then
			  write(*,*)' NO DATA IN FEATURE-FILE  !!! KEY=',key, FEATURE_FILE,'  ',imagette_id
			  P(:,:)=1
			  i_read=2 
			 else
	     	  write(*,*)' NO DATA IN FEATURE-FILE  !!! KEY=',key, FEATURE_FILE,'  ',imagette_id
			    P(:,:)=1
			    i_read=2 
			    read(*,*)iiiiiiiiiii
			 end if
			end if 
		
!===============================================================
!   mean features value over scene 
!===============================================================		
		INC_ANGLE = 0
		do i=1,9
		  INC_ANGLE = INC_ANGLE + INANGLE(i)
		enddo
		  INC_ANGLE = INC_ANGLE/9.
		
		
		PM=0
		do j=1,53
		 	ic=0
			do i=1,i_read
				if(P(1,i).gt.0.1)then 
					PM(j)=PM(j)+P(j,i)
					ic=ic+1 
				endif
			enddo
		  
			if(ic.gt.0) then
				! if(PM(j)==0)write(*,*)' ALARM',PM(j), ic,j 
        	PM(j) =PM(j)/ic
				 
				if(wv1_wv2==1)then 
					if(PM(j).ge.fmax1(j))PM(j)=fmax1(j)  
					if(PM(j).le.fmin1(j))PM(j)=fmin1(j)
				end if
				if(wv1_wv2==2)then 
					if(PM(j).ge.fmax2(j))PM(j)=fmax2(j)  
					if(PM(j).le.fmin2(j))PM(j)=fmin2(j)
				end if
				 
					if(wv1_wv2==1) PM(j) =(PM(j)-MEAN1(j))/STD1(j) 
					if(wv1_wv2==2) PM(j) =(PM(j)-MEAN2(j))/STD2(j) 
			endif
			
			! if(j==1)  write(*,*)PM(j),ic
			! if(j==53) write(*,*)PM(j),ic
		enddo
		


		if(wv1_wv2==1) mean_wind= PM(2)*STD1(2) + MEAN1(2)
		if(wv1_wv2==2) mean_wind= PM(2)*STD2(2) + MEAN2(2)
 
!=============================================================== 
!   
!---------------------------------------------------------------
	write(dum1,'(a1)')','


	do j=1,53
		if((PM(j).ge.0     ).and.(PM(j).lt.10      ))write(PMC(j),'(i4,a1,F8.6  )')j+1,':',PM(j)
		if((PM(j).ge.10    ).and.(PM(j).lt.100     ))write(PMC(j),'(i4,a1,F9.6  )')j+1,':',PM(j)
		if((PM(j).ge.100   ).and.(PM(j).lt.1000    ))write(PMC(j),'(i4,a1,F10.6 )')j+1,':',PM(j)
		if((PM(j).ge.1000  ).and.(PM(j).lt.10000   ))write(PMC(j),'(i4,a1,F11.6 )')j+1,':',PM(j)
		if((PM(j).ge.10000 ).and.(PM(j).lt.100000  ))write(PMC(j),'(i4,a1,F12.6 )')j+1,':',PM(j)
		if((PM(j).ge.100000).and.(PM(j).lt.10000000))write(PMC(j),'(i4,a1,F13.6 )')j+1,':',PM(j)
	  
		if((PM(j).lt.0      ).and.(PM(j).ge.-10      ))write(PMC(j),'(i4,a1,F9.6  )')j+1,':',PM(j)
		if((PM(j).le.-10    ).and.(PM(j).gt.-100     ))write(PMC(j),'(i4,a1,F10.6 )')j+1,':',PM(j)
		if((PM(j).le.-100   ).and.(PM(j).gt.-1000    ))write(PMC(j),'(i4,a1,F11.6 )')j+1,':',PM(j)
		if((PM(j).le.-1000  ).and.(PM(j).gt.-10000   ))write(PMC(j),'(i4,a1,F12.6 )')j+1,':',PM(j)
		if((PM(j).le.-10000 ).and.(PM(j).gt.-100000  ))write(PMC(j),'(i4,a1,F13.6 )')j+1,':',PM(j)
		if((PM(j).le.-100000).and.(PM(j).gt.-10000000))write(PMC(j),'(i4,a1,F14.6 )')j+1,':',PM(j) 	

		if(  PM(j).ge.  10000000) write(*,*)'ALARM PM(j).>.  10000000',PM(j)
		if(  PM(j).le. -10000000) write(*,*)'ALARM PM(j).<. -10000000',PM(j)

	end do 
   
!---------------------------------------------------------------
	SWHn = (SWH - 2.6)/1.2

	if((SWHn.ge.0  ).and.(SWHn.lt.10  ))write(SWH_str,'(a4, F10.8)')'  1:',SWHn
	if((SWHn.ge.10 ).and.(SWHn.lt.100 ))write(SWH_str,'(a4, F10.7)')'  1:',SWHn
	if((SWHn.lt. 0 ).and.(SWHn.gt.-10 ))write(SWH_str,'(a4, F10.7)')'  1:',SWHn
	if((SWHn.lt.-10).and.(SWHn.gt.-100))write(SWH_str,'(a4, F10.6)')'  1:',SWHn
   
!---------------------------------------------------------------  
	write(str_iS1AB,'(a6,i1)')'   55:', iS1AB

	sin_lat=sin(LAT*3.1416/180.) 
		if(sin_lat.ge.0) write(str_LAT,'(a6,F6.4)')'   56:', sin_lat
		if(sin_lat.lt.0) write(str_LAT,'(a6,F6.3)')'   56:', sin_lat

	if(INC_ANGLE.gt.30) write(str_IA,'(a6,F6.4)')'   57:',((INC_ANGLE- 35.)/(38.-35.))  ! IA normalization
	if(INC_ANGLE.lt.30) write(str_IA,'(a6,F6.4)')'   57:',((INC_ANGLE- 22.)/(25.-22.))

!===============================================================

	if (isnan(SWH)) then
		write(*,*)' SWH NaN',IDN,' ',imagette_id
	 	SWH = 0.0
	endif

	if(isnan(Tm0))then
		write(*,*)' Tm0 NaN',IDN,' ',imagette_id
		Tm0 = 0.0
	endif

	if (isnan(Tm1)) Tm1 = 0.0
	if (isnan(Tm2)) Tm2 = 0.0
	if (isnan(SW1)) SW1 = 0.0
	if (isnan(SW2)) SW2 = 0.0
	if (isnan(SWW)) SWW = 0.0
	if (isnan(Tmw)) Tmw = 0.0


	if(wv1_wv2==1)then 
		i_w1=i_w1+1

		write(212,'(f6.1,a12,53a21,3a21)')  999.9,      SWH_str,   ((PMC(j)),j=1,53),str_iS1AB,str_LAT,str_IA
	 	write(312,987)LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2,s_date,s_time,e_date,e_time,imagette_id,orbit,des_asc,' ',IDN  

	end if

	if(wv1_wv2==2)then 
		i_w2=i_w2+1

		write(222,'(f6.1,a12,56a21)')    999.9,      SWH_str,   ((PMC(j)),j=1,53),str_iS1AB,str_LAT,str_IA 
		write(322,987)LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2,s_date,s_time,e_date,e_time,imagette_id,orbit,des_asc,' ',IDN  

	 end if

	if(i_i.ne.(i_w1+ i_w2)) then
		write(*,*)'== ERROR: INPUT DATA DO NOT CORRESPOND OUTPUT DATA NUMBERS ======'  
		write(*,*)' INPUT:', i_i,        i_i1, i_i2, IDN, imagette_id 
		write(*,*)'OUTPUT:',i_w1+ i_w2,  i_w1, i_w2
		write(*,*)'   SWH=', SWH
		write(*,*)'   SWH=', SWH_str
		write(*,*)'  WIND=', mean_wind
		write(*,*)'  key=',  key
		read(*,*)iiiiiiiii 

	end if

987 FORMAT(10F12.5,6i12,a70,i10,i14,a4,a80) 
!=============================================================== 
100    continue	   ! read Scene (ID) 
	close(11)   ! close product _all-parameters_seastate.txt
	ELSE
	WRITE(*,*)"=============================================================================="
	WRITE(*,*)" ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !"
	WRITE(*,*)"INPUT FILE <ID/cci/ID_results.txt> DOES NOT EXIST AND MUST BE PROCESSED BEFORE"
	WRITE(*,*)" ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !"
	WRITE(*,*)"=============================================================================="
	ENDIF
!============================================================ 
	close(1)
	close(212)
	close(222)
	close(312)
	close(322)
!============================================================
	open(987,file='../tmp/tmp_data/year_month.txt')
	write(987,*)year
	write(987,*)month
	close(987)
!============================================================
write(*,*)' TOTAL IMAGETTES:',i_i
write(*,*)'            wv-1:',i_i1
write(*,*)'            wv-2:',i_i2
!============================================================
stop
end


!============================================================
                     PROGRAM POSTPROCESS
!============================================================
character,save:: dum1*1,dum20*20,IDN*72  
integer IO,IOO, IOOO,IOOOO
real LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,SWH_TSVM,SWH_s1,GT,wind,NRCS
integer key,wv1_wv2, s_date,s_time,e_date,e_time,orbit,des_asc
character scene_ID*64,dum10*10,dum10_1*10,dum10_2*10,dum10_3*10
real p0,p1,p2,p3, windd
integer i_i1, i_data,i_tsvm,ip,i_w,i_under30,iplus
character (len=250) :: FILE_TSVM1, FILE_DATA1, FILE_OUT1,FILE_FEATURES
!=============================================================================================
	i_data=0
	i_tsvm=0
	i_w = 0 
	i_under30=0
	iplus=0
!=============================================================================================
	FILE_DATA1 = '../tmp/tmp_data/results-and-id_wv2_swh_tmp.txt'
	FILE_TSVM1 = '../tmp/tsvm_prediction/tsvm_predicted_wv2.txt'
	FILE_OUT1  = '../tmp/tmp_results/results_wv2.txt'
	FILE_FEATURES='../tmp/tvsm_input/tsvm_input_wv2_swh_tmp.txt'
!=============================================================================================

	open(11, file=FILE_DATA1)
		do 223
		read(11,*,IOSTAT=IOOO ) dum1
		if (IOOO /= 0)exit
		i_data =  i_data + 1 
223 continue  
	close(11)
  
	open(111, file=FILE_TSVM1)
		do 123
		read(111,*,IOSTAT=IOOOO ) dum1
		if (IOOOO /= 0)exit
		i_tsvm = i_tsvm + 1 
123 continue
	close(111)
  

	if(i_data.ne.i_tsvm)then
		write(*,*)' NUMBERS OF DATA AND PREDICTED SWH DO NOT MATCH!!!! ERROR !!!!'
		read(*,*)iiii
	end if

	i_data=0
	i_tsvm=0

	open(11,   file=FILE_DATA1)                     ! /tmp/         sea state fields  + ID
	open(111,  file=FILE_TSVM1)                     ! /tsvm_out/    tsvm results 
	open(1111, file=FILE_OUT1)                      ! /results/     sum results - update sea state fields  + ID by tsvm SWH
	open(11111,file=FILE_FEATURES)
!=============================================================================================
!  DATA
!=============================================================================================
	do 100
	read(11,*,IOSTAT=IO )LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2,s_date,s_time,e_date,e_time,scene_ID,orbit,des_asc,IDN    ! read _all-parameters_seastate.txt
		if (IO /= 0)exit
		
		read(11111,*)p0,dum10_1,dum10_2,dum10_3
		read(dum10_1(3:10),*)p1
		read(dum10_2(3:10),*)p2
		read(dum10_3(3:10),*)p3
		windd = p3*3.6072349 + 9.2577657

		i_data = i_data +1 

		read(111, *,IOSTAT=IOO)SWH_TSVM 	
		if (IOO /= 0)exit
!=============================================================================================
! FILTERING
!=============================================================================================	

		if((key.ge.0).and.(SWH_TSVM.le.0.05).and.(SWH.gt.0.05).and.(SWH.lt.16))SWH_TSVM = SWH          ! 1. SWH<0.05,   key>=0, SWH<SWH_old, SWH_old<16   SWH=SWH_old

		if((SWH_TSVM.le.0.0))then                                                                      ! 2. SWH<0  
			if((SWH.gt.0).and.(SWH.le.5)) then 
				SWH_TSVM = SWH
			else
				SWH_TSVM = 0.0
				key=-2
			endif
		end if 

		if((windd.le.7.5 ).and.(SWH_TSVM.ge.6.5 ))then                                                 ! wind<7.5, SWH > 6.5 
			SWH_TSVM = 0.33*SWH_TSVM 
			i_w=i_w+1
	    end if

		if((SWH_TSVM.lt.0.30).and.(key.ge.0))then                                                     ! SWH<0.30 
			SWH_TSVM=0.
			key=-2
			i_under30=i_under30+1
   	    end if
		
		if(SW1.gt.SWH_TSVM )   SW1 = 0.9 * SWH_TSVM;
		if(SWW.gt.SWH_TSVM )   SWW = 0.9 * SWH_TSVM
		if(SW2.gt.SW1      )   SW2 = 0.9 * SW1

	!write(*,*)' OUT:', IDN
	write(1111,991) LAT,LON,SWH_TSVM,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2, &
               	    s_date,s_time,e_date,e_time,scene_ID,orbit,des_asc,' ',IDN
991 FORMAT(10F12.5,6i12,a70,i10,i14,a5,a72) 	   
100 continue
	close(11)
	close(111)
	close(1111)
	close(11111)
	
	!write(*,*)'DATA FROM TSVM AND RESULTS COUPLED  HIGH SEA ALL WINDS WV-2: OK',i_data,'/',i_tsvm
	

	!write(*,*)'================================================================================'
	write(*,*)             ' SWH REPLACED WITH TSVM-SWH  WV-2:',i_data
	if(i_w.gt.0)write(*,*) '      FILTERING WIND/SEESTATEwv-2:', i_w
	!write(*,*)'================================================================================'
	! write(*,*)'new not zero=',iplus

stop
end 

